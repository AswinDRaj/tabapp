'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var browserSync = require('browser-sync');
var browserSyncSpa = require('browser-sync-spa');
var modRewrite = require('connect-modrewrite');
// var cookie = require('cookie');

var util = require('util');

var proxyMiddleware = require('http-proxy-middleware');

function browserSyncInit(baseDir, browser) {
    browser = browser === undefined ? 'default' : browser;

    var routes = null;
    if (baseDir === conf.paths.src || (util.isArray(baseDir) && baseDir.indexOf(conf.paths.src) !== -1)) {
        routes = {
            '/bower_components': 'bower_components'
        };

    }
    var cookie;
    var server = {
        baseDir: baseDir,
        routes: routes
    };
    // server.proxy = {
    //     proxy: {
    //         proxyReq: [
    //             function(proxyReq) {
    //                 if (cookie) {
    //                     proxyReq.setHeader('Set-Cookie', cookie);
    //                     console.log("Set-Cookie"+ cookie);
    //                   }
    //             }
    //         ],
    //         proxyRes: [
    //             function(proxyRes, req, res) {
    //                  var proxyCookie = proxyRes.headers['set-cookie'];
    //                   if (proxyCookie) {
    //                     cookie = proxyCookie;
    //                     console.log("getSet-cookie"+ cookie);
    //                   }
    //             }
    //         ]
    //     }
    // };
    /*
     * You can add a proxy to your backend by uncommenting the line below.
     * You just have to configure a context which will we redirected and the target url.
     * Example: $http.get('/users') requests will be automatically proxified.
     *
     * For more details and option, https://github.com/chimurai/http-proxy-middleware/blob/v0.9.0/README.md
     */
   // var cookie;

   //  function relayRequestHeaders(proxyReq, req, res) {
   //      if (cookie) {
   //          proxyReq.setHeader('Set-Cookie', cookie);
   //          console.log("Set-Cookie"+ cookie);
   //        }
   //  };

   //  function relayResponseHeaders(proxyRes, req, res) {
   //      var proxyCookie = proxyRes.headers['set-cookie'];
   //        if (proxyCookie) {
   //          cookie = proxyCookie;
   //          console.log("getSet-cookie"+ cookie);
   //        }
   //  };
   //  server.middleware = proxyMiddleware(['/user', '/api'], {
   //      target: 'http://dev-tap-online.pantheonsite.io',
   //      changeOrigin: true, // for vhosted sites, changes host header to match to target's host
   //      logLevel: 'debug',
   //      ws: true,
   //      onProxyReq: relayRequestHeaders,
   //      onProxyRes: relayResponseHeaders
   //  });

    browserSync.instance = browserSync.init({
        startPath: '/',
        server: server,
        browser: browser
    });
}
browserSync.use(browserSyncSpa({
    selector: '[ng-app]' // Only needed for angular apps
}));

gulp.task('serve', ['watch'], function() {
    browserSyncInit([path.join(conf.paths.tmp, '/serve'), conf.paths.src]);
});

gulp.task('serve:dist', ['build'], function() {
    browserSyncInit(conf.paths.dist);
});

gulp.task('serve:e2e', ['inject'], function() {
    browserSyncInit([conf.paths.tmp + '/serve', conf.paths.src], []);
});

gulp.task('serve:e2e-dist', ['build'], function() {
    browserSyncInit(conf.paths.dist, []);
});
