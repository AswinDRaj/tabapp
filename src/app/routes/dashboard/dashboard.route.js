(function () {
  'use strict';

  angular
    .module('tapWeb')
    .config(routes)

  /** @ngInject */

  function routes($stateProvider, $urlRouterProvider,$locationProvider) {

    $stateProvider

    /**
     * Main Dashboard State
     */
      .state('landing.dashboard', {
        url: '/dashboard',
        templateUrl: 'app/templates/dashboard/dashboard.html',
        controller: 'DashboardController',
        controllerAs: 'dashboard',
        data : {
          pageTitle: 'Dashboard'
        },
        resolve: {
          isLoggedIn: isLoggedIn
        }
      })

      .state('landing.dashboard.trending', {
        url: '/trending?pageOffset',
        templateUrl: 'app/templates/dashboard/dashboard.trending.html',
        controller: 'DashboardTrendingController',
        controllerAs: 'dashtrending',
        data : {
          pageTitle: 'Dashboard Trending'
        },
        params: {
          pageOffset: '0'
        },
        resolve: {
          dashtrendingItemsResponse: getDashTrendingItems
        }
      })

      .state('landing.dashboard.ballotbox', {
        url: '/ballotbox?pageOffset',
        templateUrl: 'app/templates/dashboard/dashboard.ballotbox.html',
        controller: 'DashboardBallotBoxController',
        controllerAs: 'dashballotbox',
        data : {
          pageTitle: 'Dashboard Ballotbox'
        },
        params: {
          pageOffset: '0'
        },
        resolve: {
          dashballotBoxItemsResponse: getDashBallotBoxItems
        }
      })

      .state('landing.dashboard.timeline', {
        url: '/timeline?pageOffset',
        templateUrl: 'app/templates/dashboard/dashboard.timeline.html',
        controller: 'DashboardTimelineController',
        controllerAs: 'dashtimeline',
        data : {
          pageTitle: 'Dashboard Timeline'
        },
        params: {
          pageOffset: '0'
        },
        resolve: {
          dashtimelineItemsResponse: getDashTimelineItems
        }
      });

      $urlRouterProvider.when('landing.dashboard', 'landing/dashboard/trending');
      $locationProvider.html5Mode(true);

    /**
     * Function to check if the user is not logged in
     * @return {Promise}
     */

    // /** @ngInject */
    // function isNotLoggedIn(UserService, $q) {
    //   var deferred = $q.defer();
    //   UserService.checkLoginStatus()
    //     .then(function (response) {
    //       var status = parseInt(response.data);
    //       if (status === 1) {
    //         deferred.reject();
    //       }
    //       if (status === 0) {
    //         deferred.resolve();
    //       }
    //     }, function (err) {
    //       deferred.reject(err);
    //     });

    //   return deferred.promise;
    // }

    /**
     * Function to check if the user is not logged in
     * @return {Promise}
     */

    /** @ngInject */
    function isLoggedIn(UserService, $q) {
      var deferred = $q.defer();
      UserService.checkLoginStatus()
        .then(function (response) {
          var status = parseInt(response.data);
          if (status === 1) {
            deferred.resolve();
          }
          if (status === 0) {
            deferred.reject();
          }
        }, function (err) {
          deferred.reject(err);
        });

      return deferred.promise;
    }


    /**
     * getDashTrendingItems
     */
    /** @ngInject */
    function getDashTrendingItems(DashboardService, $stateParams) {
      return DashboardService.getDashboardTrending($stateParams.pageOffset);
    }

    /**
     * getDashBallotBoxItems
     */
    /** @ngInject */
    function getDashBallotBoxItems(DashboardService, $stateParams) {
      return DashboardService.getDashboardBallotBox($stateParams.pageOffset);
    }

    /**
     * getDashTimelineItems
     */
    /** @ngInject */
    function getDashTimelineItems(DashboardService, $stateParams) {
      return DashboardService.getDashboardTimeline($stateParams.pageOffset);
    }

  }
})();