(function() {
  'use strict';

  angular
    .module('tapWeb')
    .config(mainRoutes)

  /** @ngInject */

  function mainRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('login', {
        url: '/user/login',
        cache: false,
        templateUrl: 'app/templates/users/login.html',
        controller: 'UserController',
        controllerAs: 'users',
        data : {
          pageTitle: 'Login'
        },
        resolve: {
          userLoggedIn: isNotLoggedIn
        }
      })
      .state('register', {
        url: '/user/register',
        cache: false,
        templateUrl: 'app/templates/users/register.html',
        controller: 'UserController',
        controllerAs: 'users',
        data : {
          pageTitle: 'Register'
        },
        resolve: {
          userLoggedIn: isNotLoggedIn
        }
      })
      .state('forgot', {
        url: '/user/forgotpassword',
        cache: false,
        templateUrl: 'app/templates/users/forgotpassword.html',
        controller: 'UserController',
        controllerAs: 'users',
        data : {
          pageTitle: 'Forgot Password'
        },
        resolve: {
          userLoggedIn: isNotLoggedIn
        }
      })
      .state('reset', {
        url: '/user/reset/:userid/:timestamp/:userToken',
        cache: false,
        templateUrl: 'app/templates/users/resetpassword.html',
        controller: 'UserController',
        controllerAs: 'users',
        data : {
          pageTitle: 'Reset Password'
        }
      })
      .state('landing.userprofile', {
        url: '/userprofile',
        templateUrl: 'app/templates/users/userprofile.html',
        controller: 'UserController',
        controllerAs: 'users',
        data : {
          pageTitle: 'User Profile'
        },
        resolve: {
          isLoggedIn: isLoggedIn
        }
      })
      .state('landing.userprofile.camera', {
        url: '/camera',
        templateUrl: 'app/templates/users/capture.html',
        controller: 'UserController',
        controllerAs: 'users',
        data : {
          pageTitle: 'Web Camera'
        },
        resolve: {
          isLoggedIn: isLoggedIn
        }
      })
      .state('landing.viewuser', {
        url: '/user/:userid',
        templateUrl: 'app/templates/users/otherprofile.html',
        controller: 'UserController',
        controllerAs: 'users',
        data : {
          pageTitle: 'Other User Profile'
        },
        resolve: {
          isLoggedIn: isLoggedIn
        }
      })
      .state('logout', {
        url: '/logout',
        controller: 'UserLogoutController',
        controllerAs: 'logout',
        data : {
          pageTitle: 'Logout'
        },
        resolve: {
          isLoggedIn: isLoggedIn
        }
      });

    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);

    /**
     * Function to check if the user is not logged in
     * @return {Promise}
     */

    /** @ngInject */
    function isNotLoggedIn(UserService, $q) {
      var deferred = $q.defer();
      UserService.checkLoginStatus()
        .then(function(response) {
          var status = parseInt(response.data);
          if (status === 1) {
            deferred.reject();
          }
          if (status === 0) {
            deferred.resolve();
          }
        }, function(err) {
          deferred.reject(err);
        });

      return deferred.promise;
    }

    /**
     * Function to check if the user is not logged in
     * @return {Promise}
     */

    /** @ngInject */
    function isLoggedIn(UserService, $q) {
      var deferred = $q.defer();
      UserService.checkLoginStatus()
        .then(function(response) {
          var status = parseInt(response.data);
          if (status === 1) {
            deferred.resolve();
          }
          if (status === 0) {
            deferred.reject();
          }
        }, function(err) {
          deferred.reject(err);
        });

      return deferred.promise;
    }

  }

})();
