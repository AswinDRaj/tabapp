(function() {
  'use strict';

  angular
    .module('tapWeb')
    .config(followRoutes)

  /** @ngInject */

  function followRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('landing.follow', {
        url: '',
        templateUrl: 'app/templates/users/users.html',
        controller: 'FollowController',
        controllerAs: 'follow',
        resolve: {
          isLoggedIn: isLoggedIn
        }
      })
      .state('landing.follow.followers', {
        url: '/users/:userid/followers?pageOffset?search_query',
        templateUrl: 'app/templates/users/users.followers.html',
        controller: 'FollowerController',
        controllerAs: 'followers',
        data : {
          pageTitle: 'User Followers'
        },
        params: {
          pageOffset: '0',
          search_query: 'all'
        },
        resolve: {
          followersItemResponse: getFollowerItems,
          isLoggedIn: isLoggedIn
        }
      })
      .state('landing.follow.following', {
        url: '/users/:userid/following?pageOffset?search_query',
        templateUrl: 'app/templates/users/users.following.html',
        controller: 'FollowingController',
        controllerAs: 'following',
        data : {
          pageTitle: 'User Following'
        },
        params: {
          pageOffset: '0',
          search_query: 'all'
        },
        resolve: {
          followingItemResponse: getFollowingItems,
          isLoggedIn: isLoggedIn

        }
      });

     $urlRouterProvider.when('landing.follow', 'landing.follow.followers');
      $locationProvider.html5Mode(true);

    /**
     * Function to check if the user is not logged in
     * @return {Promise}
     */

    // /** @ngInject */
    // function isNotLoggedIn(UserService, $q) {
    //   var deferred = $q.defer();
    //   UserService.checkLoginStatus()
    //     .then(function(response) {
    //       var status = parseInt(response.data);
    //       if (status === 1) {
    //         deferred.reject();
    //       }
    //       if (status === 0) {
    //         deferred.resolve();
    //       }
    //     }, function(err) {
    //       deferred.reject(err);
    //     });

    //   return deferred.promise;
    // }

    /**
     * Function to check if the user is not logged in
     * @return {Promise}
     */

    /** @ngInject */
    function isLoggedIn(UserService, $q) {
      var deferred = $q.defer();
      UserService.checkLoginStatus()
        .then(function(response) {
          var status = parseInt(response.data);
          if (status === 1) {
            deferred.resolve();
          }
          if (status === 0) {
            deferred.reject();
          }
        }, function(err) {
          deferred.reject(err);
        });

      return deferred.promise;
    }

     /**
     * getFollowerItems
     */
    /** @ngInject */
    function getFollowerItems(PageAuthService, $stateParams) {
      return PageAuthService.getFollowers($stateParams.userid,$stateParams.pageOffset,$stateParams.search_query);
    }

    /**
     * getFollowingItems
     */
    /** @ngInject */
    function getFollowingItems(PageAuthService, $stateParams) {
      return PageAuthService.getFollowing($stateParams.userid,$stateParams.pageOffset,$stateParams.search_query);
    }
  }

})();
