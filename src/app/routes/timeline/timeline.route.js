(function () {
  'use strict';

  angular
    .module('tapWeb')
    .config(routes)

  /** @ngInject */

  function routes($stateProvider, $urlRouterProvider,$locationProvider) {

    $stateProvider

    /**
     * Main Dashboard State
     */
      .state('landing.timeline', {
        url: '/timeline',
        templateUrl: 'app/templates/timeline/timeline.html',
        controller: 'TimelineController',
        controllerAs: 'timeline',
        data : {
          pageTitle: 'Timeline'
        },
        resolve: {
          timelinecategoryResponse: gettimelineAllcategory,
          isLoggedIn: isLoggedIn
        }
      })

      .state('landing.timeline.all', {
        url: '/all?pageOffset',
        templateUrl: 'app/templates/timeline/timeline.all.html',
        controller: 'TimelineAllController',
        controllerAs: 'timelineAll',
        data : {
          pageTitle: 'Timeline All Posts'
        },
        params: {
          pageOffset: '0'
        },
        resolve: {
          timelineAllItemsResponse: gettimelineAllItems,
          isLoggedIn: isLoggedIn
        }
      })

      .state('landing.timeline.myposts', {
        url: '/myposts?pageOffset',
        templateUrl: 'app/templates/timeline/timeline.mypost.html',
        controller: 'TimelinemyController',
        controllerAs: 'timelinemyPost',
         data : {
          pageTitle: 'Timeline My Posts'
        },
        params: {
          pageOffset: '0'
        },
        resolve: {
          timelinemyPostItemsResponse: gettimelinemyPostItems,
          isLoggedIn: isLoggedIn
        }
      })
    .state('timeline', {
        url: '/timeline/:timelineId',
        templateUrl: 'app/templates/timeline/timelineShare.html',
        controller: 'timelineShareController',
        controllerAs: 'sharetimeline',
         data : {
          pageTitle: 'Timeline Share'
        },
        resolve: {
          timelineShareItemsResponse: gettimelineShareItems,
          isLoggedIn: isLoggedIn
        }
      })

      $urlRouterProvider.when('landing.timeline', 'landing/timeline/all');
      $locationProvider.html5Mode(true);

    /**
     * Function to check if the user is not logged in
     * @return {Promise}
     */

    // /** @ngInject */
    // function isNotLoggedIn(UserService, $q) {
    //   var deferred = $q.defer();
    //   UserService.checkLoginStatus()
    //     .then(function (response) {
    //       var status = parseInt(response.data);
    //       if (status === 1) {
    //         deferred.reject();
    //       }
    //       if (status === 0) {
    //         deferred.resolve();
    //       }
    //     }, function (err) {
    //       deferred.reject(err);
    //     });

    //   return deferred.promise;
    // }

    /**
     * Function to check if the user is not logged in
     * @return {Promise}
     */

    /** @ngInject */
    function isLoggedIn(UserService, $q) {
      var deferred = $q.defer();
      UserService.checkLoginStatus()
        .then(function (response) {
          var status = parseInt(response.data);
          if (status === 1) {
            deferred.resolve();
          }
          if (status === 0) {
            deferred.reject();
          }
        }, function (err) {
          deferred.reject(err);
        });

      return deferred.promise;
    }


    /**
     * gettimelineAllItems
     */
    /** @ngInject */
    function gettimelineAllItems(TimelineService, $stateParams) {
      return TimelineService.gettimelineAllItems($stateParams.pageOffset);
    }

    /**
     * gettimelinePostItems
     */
    /** @ngInject */
    function gettimelinemyPostItems(TimelineService, $stateParams) {
      return TimelineService.gettimelinemyPostItems($stateParams.pageOffset);
    }
   /**
     * gettimelineShareItems
     */
    /** @ngInject */
    function gettimelineShareItems(TimelineService, $stateParams) {
      return TimelineService.gettimelineshareItems($stateParams.timelineId);
    }
    /**
  /**
     * gettimelinecategory
     */
    /** @ngInject */
  function gettimelineAllcategory(TimelineService) {
    return TimelineService.gettimelineCatagory();
  }
  }
})();