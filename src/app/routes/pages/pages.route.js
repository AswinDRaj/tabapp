(function() {
  'use strict';

  angular
    .module('tapWeb')
    .config(mainRoutes)

  /** @ngInject */

  function mainRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('landing', {
        url: '/landing',
        abstract: true,
        cache: false,
        templateUrl: 'app/templates/main/landing.html',
        controller: 'PageController',
        controllerAs: 'page'
      })
      .state('landing.trending', {
        url: '/trending?pageOffset',
        templateUrl: 'app/templates/trending/trending.html',
        controller: 'TrendingController',
        controllerAs: 'trending',
        data : {
          pageTitle: 'Trending'
        },
        params: {
          pageOffset: '0'
        },
        resolve: {
          trendingItemsResponse: getTrendingItems
        }
      })
      .state('landing.trendingdetails', {
        url: '/trendingDetail/:trendingId',
        templateUrl: 'app/templates/trending/trendingDetails.html',
        controller: 'TrendingDetailController',
        controllerAs: 'detailTrending',
        data : {
          pageTitle: 'Trending Detail'
        },
        resolve: {
          trendingDetailItemsResponse: getTrendingDetailItems
        }
      })
      .state('trending', {
        url: '/trending/:trendingId/:pollid',
        templateUrl: 'app/templates/trending/trendingShare.html',
        controller: 'TrendingShareController',
        controllerAs: 'shareTrending',
        data : {
          pageTitle: 'Trending Share'
        },
        resolve: {
          trendingShareItemsResponse: gettrendingShareItems
        }
      })
      .state('landing.ballotbox', {
        url: '/ballotbox?pageOffset',
        cache: false,
        templateUrl: 'app/templates/ballotbox/ballotbox.html',
        controller: 'BallotboxController',
        controllerAs: 'ballotbox',
        data : {
          pageTitle: 'Ballotbox'
        },
        params: {
          pageOffset: '0'
        },
        resolve: {
          ballotBoxItemsResponse: getBallotBoxItems
        }
      })
       .state('ballotbox', {
        url: '/ballot-box/:ballotId',
        templateUrl: 'app/templates/ballotbox/ballotShare.html',
        controller: 'ballotShareController',
        controllerAs: 'shareBallot',
        data : {
          pageTitle: 'Ballotbox Detail'
        },
        resolve: {
          ballotShareItemsResponse: getballotShareItems
        }
      })
      .state('landing.movies', {
        url: '/movies?pageOffset?movieLanguage',
        cache: false,
        templateUrl: 'app/templates/movies/movies.html',
        controller: 'MovieController',
        controllerAs: 'movies',
        data : {
          pageTitle: 'Movies'
        },
        params: {
          pageOffset: '0',
          movieLanguage:'all'
        },
        resolve: {
          movieItemsResponse: getMovieItems
        }
      })
      .state('landing.sports', {
        url: '/sports?pageOffset?sportId',
        cache: false,
        templateUrl: 'app/templates/sports/sports.html',
        controller: 'SportsController',
        controllerAs: 'sports',
        data : {
          pageTitle: 'Sports'
        },
        params: {
          pageOffset: '0',
          sportId:'all'
        },
        resolve: {
          sportsItemsResponse: getSportItems
        }
      })
      .state('landing.awards', {
        url: '/awards?pageOffset',
        templateUrl: 'app/templates/awards/awards.html',
        controller: 'AwardController',
        controllerAs: 'awards',
        data : {
          pageTitle: 'Awards'
        },
        params: {
          pageOffset: '0'
        },
        resolve: {
          awardsItemsResponse: getAwardItems
        }
      })

      .state('landing.awardsdetail', {
        url: '/awardsDetails/:awardId',
        templateUrl: 'app/templates/awards/awardsDetails.html',
        controller: 'AwarddetailController',
        controllerAs: 'awardsdetail',
        data : {
          pageTitle: 'Award Detail'
        },
        params: {
          pageOffset: '0'
        },
       resolve: {
          awardsdetailsItemsResponse: getAwarddetailItems
        }

      });



    $urlRouterProvider.otherwise('/');
    $locationProvider.html5Mode(true);


    /**
     * getTrendingItems
     */
    /** @ngInject */
    function getTrendingItems(PageAuthService, $stateParams) {
      return PageAuthService.getTrending($stateParams.pageOffset);
    }

    /**
     * getTrendingDetailItems
     */
    /** @ngInject */
    function getTrendingDetailItems(PageAuthService, $stateParams) {
      return PageAuthService.getTrendingDetail($stateParams.trendingId);
    }

    /**
     * gettrendingShareItems
     */
    /** @ngInject */
    function gettrendingShareItems(PageAuthService, $stateParams) {
      return PageAuthService.getTrendingDetail($stateParams.trendingId);
    }
    /**
     * getBallotBoxItems
     */
    /** @ngInject */
    function getBallotBoxItems(PageAuthService, $stateParams) {
      return PageAuthService.getBallotBox($stateParams.pageOffset);
    }

/**
     * gettrendingShareItems
     */
    /** @ngInject */
    function getballotShareItems(PageAuthService, $stateParams) {
      return PageAuthService.getBallotDetail($stateParams.ballotId);
    }
    /**
     * getMovieItems
     */
    /** @ngInject */
    function getMovieItems(PageAuthService, $stateParams) {
      return PageAuthService.getMovies($stateParams.pageOffset,$stateParams.movieLanguage);
    }

    /**
     * getSportItems
     */
    /** @ngInject */
    function getSportItems(PageAuthService, $stateParams) {
      return PageAuthService.getSports($stateParams.pageOffset,$stateParams.sportId);
    }

   /**
     * getAwardItems
     */
    /** @ngInject */
    function getAwardItems(PageAuthService, $stateParams) {
      return PageAuthService.getAwards($stateParams.pageOffset);
    }
  /**
     * getAwarddetailItems
     */
    /** @ngInject */
    function getAwarddetailItems(PageAuthService, $stateParams) {
      return PageAuthService.getawardDetail($stateParams.awardId);
    }
  }

})();
