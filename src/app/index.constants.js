/* global moment:false */
(function() {
        'use strict';

        angular
            .module('tapWeb')
            .constant('moment', moment);

})();
