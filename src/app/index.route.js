(function() {
  'use strict';

  angular
    .module('tapWeb')
    .config(mainRoutes)
    .factory('authHttpResponseInterceptor', function authHttpResponseInterceptor($q, $location, $rootScope) {
            return {
                response: function(response) {
                    if (response.status === 401) {
                      $rootScope.status = '401';
                    }
                    return response || $q.when(response);
                },
                responseError: function(rejection) {
                    if (rejection.status === 401) {
                        $rootScope.isLoggedIn = false;
                        $location.path('/login');
                    }else if (rejection.status === 403) {
                      //alert(rejection.data.errors[0].detail);
                    }else if (rejection.status >= 500) {
                        $rootScope.isServerError = true;
                    }else if (rejection.status === 404) {
                        $location.path('/404');
                    }
                    return $q.reject(rejection);
                }
            }
        });

  /** @ngInject */

  function mainRoutes($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        cache: false,
        templateUrl: 'app/templates/main/main.html',
        controller: 'MainController',
        controllerAs: 'main',
        data : {
          pageTitle: 'Home'
        }
      })
      .state('404', {
        url: '/page-not-found',
        cache: false,
        templateUrl: 'app/templates/main/404.html',
        data : {
          pageTitle: 'Page not found'
        }
      })
      .state('webprivacypolicy', {
        url: '/web/privacypolicy',
        cache: false,
        templateUrl: 'app/templates/main/privacypolicy.html',
        data : {
          pageTitle: 'Privacy Policy'
        }
      })
      .state('webtermsandconditions', {
        url: '/web/termsandconditions',
        cache: false,
        templateUrl: 'app/templates/main/terms.html',
        data : {
          pageTitle: 'Terms and Conditions'
        }
      })
      .state('privacypolicy', {
        url: '/privacypolicy',
        cache: false,
        templateUrl: 'app/templates/privacypolicy.html',
        data : {
          pageTitle: 'Privacy Policy'
        }
      })
      .state('termsandconditions', {
        url: '/termsandconditions',
        cache: false,
        templateUrl: 'app/templates/terms.html',
        data : {
          pageTitle: 'Terms and Conditions'
        }
      });
    $urlRouterProvider.otherwise('/page-not-found');
    $locationProvider.html5Mode(true);
  }

})();
