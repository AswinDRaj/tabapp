(function () {
  'use strict';
	angular.module('tapWeb', [
    'ngAnimate',
    'ngCookies',
    'ngTouch',
    'moment-picker',
    'naif.base64',
    'ngSanitize',
    'ngStorage',
    'nvd3',
    'ngMessages',
    'ngResource',
    'ngAria',
    'ui.router',
    'ui.bootstrap',
    'toastr',
    'ngProgress',
    'camera'
]);


})();
