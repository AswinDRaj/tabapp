(function() {
    'use strict';

    angular
        .module('tapWeb')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log, $rootScope, $window, ngProgressFactory) {
      var events = {};
      $log.debug('runBlock end');

      $rootScope.progressbar = ngProgressFactory.createInstance();


      events.stateChangeStart = $rootScope.$on('$stateChangeStart', function() {
        $rootScope.progressbar.start();
      });
      events.stateChangeSuccess = $rootScope.$on('$stateChangeSuccess', function(event, toState) {
        $rootScope.bodyClass = toState.name.replace(/\./g, '-').replace('landing-', '');
        $rootScope.pageTitle = toState.data.pageTitle;
        $rootScope.webUrl = event.targetScope.host;
        $rootScope.progressbar.complete();
      });
      events.stateChangeError = $rootScope.$on('$stateChangeError', function() {
        $rootScope.progressbar.reset();
      });

      $rootScope.themeDir = $window.themeDir ? $window.themeDir : '';
      $rootScope.host = $window.tapConfig.host;
    }
})();
