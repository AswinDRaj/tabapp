(function() {
    'use strict';

    angular.module('tapWeb')
        .config(function($httpProvider, $logProvider, toastrConfig) {
            /** @ngInject */
            // Enable log
            $logProvider.debugEnabled(true);
            // Set options third-party lib
            toastrConfig.allowHtml = true;
            toastrConfig.preventDuplicates = true;
            $httpProvider.defaults.withCredentials = true;
           $httpProvider.defaults.headers.common['Content-Type'] = ''
            + 'application/json; charset=UTF-8';
            $httpProvider.interceptors.push('authHttpResponseInterceptor');
        })
        .factory('Authenticate', function() {
            return {
                setLocalStorage: function(username, csrftoken, userid, logouttoken) {
                    if (angular.isDefined(Storage)) {
                        var authDataInit = {},
                            authSetInit;
                        var authGet = angular.fromJson(localStorage.getItem("tapWeb"));
                        if (authGet === null) {
                            authDataInit.username = '';
                            authDataInit.csrftoken = '';
                            authDataInit.userid = 0;
                            authDataInit.logouttoken = 0;
                            authSetInit = angular.toJson(authDataInit);
                            localStorage.setItem("tapWeb", authSetInit);
                        } else {
                            if (angular.isUndefined(username)) {
                                authDataInit.username = '';
                            } else {
                                authDataInit.username = username;
                            }
                            if (angular.isUndefined(csrftoken)) {
                                authDataInit.csrftoken = '';
                            } else {
                                authDataInit.csrftoken = csrftoken;
                            }
                            if (angular.isUndefined(userid)) {
                                authDataInit.userid = 0;
                            } else {
                                authDataInit.userid = userid;
                            }
                            if (angular.isUndefined(logouttoken)) {
                                authDataInit.logouttoken = 0;
                            } else {
                                authDataInit.logouttoken = logouttoken;
                            }
                            authSetInit = angular.toJson(authDataInit);
                            localStorage.setItem("tapWeb", authSetInit);
                        }
                    }

                },
                emptyLocalStorage: function() {
                     if (angular.isDefined(Storage)) {
                        var authDataInit = {},
                            authSetInit;
                        authDataInit.csrftoken = '';
                        authDataInit.userid = '';
                        authDataInit.username = '';
                        authDataInit.logouttoken = 0;
                        authSetInit = angular.toJson(authDataInit);
                        localStorage.setItem("tapWeb", authSetInit);
                    }
                    //api.init(0);
                },
                getLocalStorage: function() {
                      if (angular.isDefined(Storage)) {
                        var authGet = angular.fromJson(localStorage.getItem("tapWeb"));
                        if (authGet === null) {
                            return { 'username': '', 'csrftoken': '', 'userid': 0, 'logouttoken': 0 }
                        } else {
                            return { 'username': authGet.username, 'csrftoken': authGet.csrftoken, 'userid': authGet.userid, 'logouttoken': authGet.logouttoken}
                        }
                    }
                }
            };

        })
})();
