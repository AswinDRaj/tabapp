(function () {
  'use strict';

  angular.module('tapWeb')
    .run(AdminRedirect)
    .factory('UserService', UserService);

  /** @ngInject */
  function UserService(CSRF, $window, $log, $http, $q, $location, $rootScope, $cookies, $timeout, Authenticate) {
    /** @ngInclude */
    var service = {};
    $rootScope.csrf = null;
    /**
     * Login Service
     * @param username
     * @param password
     * @param callback
     * @returns {Session Details}
     * @constructor
     */
    service.Login = function (username, password, callback) {
      var deferred = $q.defer();
      CSRF.getToken()
        .then(function (response) {
          $http({
            'method': $window.tapConfig.apis.user.login.method,
            'url': $window.tapConfig.host + $window.tapConfig.apis.user.login.url,
            'data': {name: username, pass: password},
            'headers': {
              'Content-Type': 'application/json; charset=UTF-8',
              'X-CSRF-TOKEN': response.data.token
            }
          }).success(function (data) {
            $rootScope.isLoggedIn = true;
            Authenticate.setLocalStorage(data.current_user.name, data.csrf_token, data.current_user.uid, data.logout_token);
            callback(data);
            deferred.resolve(data);
          }).error(function (data) {
            $rootScope.isLoggedIn = false;
            callback(data);
            deferred.reject(data);
          });

        }).catch(function (err) {
        $log.error(err);
      });
      return deferred.promise;
    };

    // /**
    //  * Get User Object
    //  * @retuns {Promise}
    //  */
    // service.getUser = function() {
    //   return $http({
    //     'method': $window.tapConfig.apis.user.load_user.method,
    //     'url': $window.tapConfig.host + 'api/v1/user?_format=json'
    //   });
    // };

    /**
     * Get User By ID
     * @param uid
     * @retuns {Promise}
     */
    service.getUserById = function (uid) {
      return $http({
        'method': $window.tapConfig.apis.user.load_user.method,
        'url': $window.tapConfig.host + $window.tapConfig.apis.user.load_user.url.replace('{{user_id}}', uid)
      });
    };

    /**
     * Search Users
     * @param field_full_name
     * @retuns {other User}
     */
    service.getUserPrf = function (questionname, paginate, callback) {
      var deferred = $q.defer();
      $http({
        method: $window.tapConfig.apis.search.users.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.search.users.url.replace('{{search_string}}',questionname).replace('{{page.offset}}',paginate)
      }).success(function (data) {
        deferred.resolve(data);
        callback(data);
      }).error(function (data) {
        callback(data);
        deferred.reject();
      });
    };

    /**
     * Change Register User
     */
    service.registerAcc = function (userReg, callback) {
      var deferred = $q.defer();
      $http({
        method: $window.tapConfig.apis.user.register.method,
        data: userReg,
        url: $window.tapConfig.host + $window.tapConfig.apis.user.register.url,
        headers: {
          'Content-Type': 'application/json'
        }
      }).success(function (response) {
        $rootScope.isRegistered = true;
        deferred.resolve(response);
        callback(response);

      }).error(function (data) {
         $rootScope.isRegistered = false;
        deferred.resolve(data);
        callback(data);
      });
    };

    /**
     * [changePass description]
     * @param  {[type]}   userId      [description]
     * @param  {[type]}   currentpass [description]
     * @param  {[type]}   newpass     [description]
     * @param  {Function} callback    [description]
     * @return {[type]}               [description]
     */
    service.changePass = function (userId, currentpass, newpass, callback) {
      var deferred = $q.defer();
      CSRF.getToken()
        .then(function (response) {
          $http({
            method: $window.tapConfig.apis.user.change_pass.method,
            data: {current_pass: currentpass, pass: newpass},
            url: $window.tapConfig.host + $window.tapConfig.apis.user.change_pass.url.replace('{{user_id}}',userId),
            headers: {
              'Content-Type': 'application/json',
              'X-CSRF-TOKEN': response.data.token
            }
          }).success(function (data) {
            deferred.resolve(data);
            callback(data);
          }).error(function (data) {
            callback(data);
            deferred.reject();
          });
        }).catch(function (err) {
        $log.error(err);
      });
      return deferred.promise;
    };
    /**
     * [change_pass_web description]
     * @param  {[type]}   userId    [description]
     * @param  {[type]}   timestamp [description]
     * @param  {[type]}   hash      [description]
     * @param  {[type]}   password  [description]
     * @param  {Function} callback  [description]
     * @return {[type]}             [description]
     */
    service.change_pass_web = function(userId,timestamp,hash,password,callback){
      var deferred = $q.defer();
      CSRF.getToken()
        .then(function (response) {
          $http({
            method: $window.tapConfig.apis.user.change_pass_web.method,
            url: $window.tapConfig.host + $window.tapConfig.apis.user.change_pass_web.url.replace('{{user_id}}',userId).replace('{{timestamp}}',timestamp).replace('{{hash}}',hash),
            data:{pass:password},
            headers: {
              'Content-Type': 'application/json',
              'X-CSRF-TOKEN': response.data.token
            }
          }).success(function (data) {
            deferred.resolve(data);
            callback(data);
          }).error(function (data) {
            callback(data);
            deferred.reject();
          });
        }).catch(function (err) {
        $log.error(err);
      });
      return deferred.promise;
    };

    /**
     * Check if a user is logged in or not
     * @returns {Promise}
     */
    service.checkLoginStatus = function () {
      return $http({
        method: 'GET',
        url: $window.tapConfig.host + 'user/login_status?_format=json'
      });
    };


    /**
     * Logout
     */
    service.logout = function (logoutToken, callback) {
      var deferred = $q.defer();
      CSRF.getToken()
        .then(function (response) {
          $http({
            'method': $window.tapConfig.apis.user.logout.method,
            'url': $window.tapConfig.host + $window.tapConfig.apis.user.logout.url,
            'headers': {
              'Content-Type': 'application/json; charset=UTF-8',
              'X-CSRF-TOKEN': response.data.token
            }
          }).success(function (data) {
            deferred.resolve(data);
            callback(data);
          }).error(function (data) {
            callback(data);
            deferred.reject();
          });
        }).catch(function (err) {
          $log.error(err);
      });
    };

    /**
     * [resetPass description]
     * @param  {[type]}   email    [description]
     * @param  {Function} callback [description]
     * @return {[type]}            [description]
     */
    service.resetPass = function (email, callback) {
      CSRF.getToken()
        .then(function (response) {
          $http({
        method: $window.tapConfig.apis.user.reset_pass.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.user.reset_pass.url,
        data: {email: email},
        'headers': {
          'Content-Type': 'application/json; charset=UTF-8',
          'X-CSRF-TOKEN': response.data.token
        }
       }).success(function (data) {
            callback(data);
          }).error(function (data) {
            callback(data);
          });
        }).catch(function (err) {
          $log.error(err);
      });
    };

    /**
     * [changePicture description]
     * @param  {[type]}   userid   [description]
     * @param  {[type]}   passImg  [description]
     * @param  {Function} callback [description]
     * @return {[type]}            [description]
     */
    service.changePicture = function (userid, passImg, callback) {
      CSRF.getToken()
        .then(function (response) {
          $http({
            method: $window.tapConfig.apis.user.update_picture.method,
            data: {user_picture: passImg},
            url: $window.tapConfig.host + $window.tapConfig.apis.user.update_picture.url.replace('{{user_id}}', userid),
            headers: {
              'Content-Type': 'application/json; charset=UTF-8',
              'X-CSRF-TOKEN': response.data.token
            }
          }).success(function (data) {
            callback(data);
          }).error(function (data) {
            callback(data);
          });
        });
    };

    /**
     * [updatePrf description]
     * @param  {[type]}   userid         [description]
     * @param  {[type]}   updateName     [description]
     * @param  {[type]}   dob            [description]
     * @param  {[type]}   updateCountry  [description]
     * @param  {[type]}   updateState    [description]
     * @param  {[type]}   updateInterest [description]
     * @param  {Function} callback       [description]
     * @return {[type]}                  [description]
     */
    service.updatePrf = function (userid, updateName, dob, updateCountry, updateState, updateInterest, callback) {
      CSRF.getToken()
        .then(function (response) {
          $http({
            method: $window.tapConfig.apis.user.update_profile.method,
            url: $window.tapConfig.host + $window.tapConfig.apis.user.update_profile.url.replace('{{user_id}}', userid),
            data: {
              field_full_name: updateName,
              field_date_of_birth: dob,
              field_country: updateCountry,
              field_state: updateState,
              field_interests: updateInterest
            },
            headers: {
              'Content-Type': 'application/json',
              'X-CSRF-TOKEN': response.data.token
            }
          }).success(function (data) {
            callback(data);
          }).error(function (data) {
            callback(data);
          });
        });
    };

    /**
     * [FollowPost description]
     * @param {[type]}   followid [description]
     * @param {Function} callback [description]
     */
    service.FollowPost = function (followid, callback) {
      CSRF.getToken()
        .then(function (response) {
          $http({
            method: $window.tapConfig.apis.follow.follow.method,
            url: $window.tapConfig.host + $window.tapConfig.apis.follow.follow.url.replace('{{user_id}}', followid),
            'headers': {
              'Content-Type': 'application/json; charset=UTF-8',
              'X-CSRF-TOKEN': response.data.token
            }
          }).success(function (data) {
            callback(data);
          }).error(function (data) {
            callback(data);
          });
      }).catch(function (err) {
          $log.error(err);
      });
    };

    /**
     * [unFollowPost description]
     * @param  {[type]}   followid [description]
     * @param  {Function} callback [description]
     * @return {[type]}            [description]
     */
    service.unFollowPost = function (followid, callback) {
      CSRF.getToken()
        .then(function (response) {
          $http({
            method: $window.tapConfig.apis.follow.unfollow.method,
            url: $window.tapConfig.host + $window.tapConfig.apis.follow.unfollow.url.replace('{{user_id}}', followid),
            'headers': {
              'Content-Type': 'application/json; charset=UTF-8',
              'X-CSRF-TOKEN': response.data.token
            }
          }).success(function (data) {
            callback(data);
          }).error(function (data) {
            callback(data);
          });
         }).catch(function (err) {
          $log.error(err);
      });
    };
    /**
     * [userSearch description]
     * @param  {[type]} search_user [description]
     * @return {[type]}             [description]
     */
    service.userSearch = function(search_user){
        return $http({
        method: $window.tapConfig.apis.search.users.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.search.users.url.replace('{{search_string}}', search_user).replace('{{page.limit}}', '20').replace('{{page.offset}}', '0')
      });
    };
    /**
     * [getCategories description]
     * @return {[type]} [description]
     */
    service.getCategories = function () {
      return $http({
        method: $window.tapConfig.apis.categories.interests.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.categories.interests.url
      });
    };
    return service;
  }

  /** @ngInject */
  function AdminRedirect($rootScope, $window) {
    var onUserLogin = $rootScope.$on('USER_PROFILE_LOADED', function(event, response) {
      var user = response.data;
      if (user.roles) {
        for(var i = 0; i < user.roles.length ; i++) {
          switch (user.roles[i].target_id) {
            case 'administrator':
            case 'moderator':
            case 'manager':
              $window.location.href = $window.tapConfig.host;
              break;
            default:
              break;
          }
        }
      }
    });
  }

})();
