(function () {
  'use strict';

  angular.module('tapWeb')
    .service('DashboardService', DashboardService)


  /** @ngInject */
  function DashboardService($log, $http, $window) {


    this.getDashboardTrending = function (pageOffset) {
      return $http({
        method: $window.tapConfig.apis.dashboard.trending.list.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.dashboard.trending.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset)
      });
    };

    this.getDashboardTimeline = function (pageOffset) {
      return $http({
        method: $window.tapConfig.apis.dashboard.timeline.list.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.dashboard.timeline.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset)
      });
    };

    this.getDashboardBallotBox = function (pageOffset) {
      return $http({
        method: $window.tapConfig.apis.dashboard.ballot_box.list.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.dashboard.ballot_box.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset)
      });
    };

  }


})();
