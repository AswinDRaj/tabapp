(function() {
  'use strict';

  angular
    .module('tapWeb')
    .service('CSRF', CSRF);


  /** @ngInject */
  function CSRF($http, $window) {
    this.getToken = function() {
      return $http({
        'type' : 'post',
        'url' : $window.tapConfig.host +'api/v0/csrf?_format=json'
      });
    };
  }

})();
