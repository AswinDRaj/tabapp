(function() {
    'use strict';

    angular.module('tapWeb')
    .service('PageAuthService',PageAuthService)

    /** @ngInject */
    function PageAuthService(CSRF, $log, $http,$window) {
                /**
                 * [getTrending description]
                 * @param  {[type]} pageOffset [description]
                 * @return {[type]}            [description]
                 */
                this.getTrending = function(pageOffset) {
                    return $http({
                        method: $window.tapConfig.apis.trending.list.method,
                        url: $window.tapConfig.host +$window.tapConfig.apis.trending.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset)
                    });
                };
                /**
                 * [getTrendingDetail description]
                 * @param  {[type]} trendingid [description]
                 * @return {[type]}            [description]
                 */
                this.getTrendingDetail = function(trendingid) {
                    return $http({
                        method: $window.tapConfig.apis.trending.item.method,
                        url: $window.tapConfig.host +$window.tapConfig.apis.trending.item.url.replace('{{id}}',trendingid)
                    });

                };
                /**
                 * [getBallotBox description]
                 * @param  {[type]} pageOffset [description]
                 * @return {[type]}            [description]
                 */
                this.getBallotBox = function(pageOffset) {
                    return $http({
                        method: $window.tapConfig.apis.ballot_box.list.method,
                        url: $window.tapConfig.host + $window.tapConfig.apis.ballot_box.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset)
                    });
                };
                /**
                 * [getBallotDetail description]
                 * @param  {[type]} getUuid [description]
                 * @return {[type]}         [description]
                 */
                this.getBallotDetail = function(getUuid) {
                    return $http({
                        method: $window.tapConfig.apis.ballot_box.item.method,
                        url: $window.tapConfig.host + $window.tapConfig.apis.ballot_box.item.url.replace('{{id}}',getUuid)
                    });
                };
                /**
                 * [getMovies description]
                 * @param  {[type]} pageOffset [description]
                 * @param  {[type]} languageId [description]
                 * @return {[type]}            [description]
                 */
                this.getMovies = function(pageOffset,languageId) {
                    if(languageId === 'all'){
                        return $http({
                            method: $window.tapConfig.apis.movies.list.method,
                            url: $window.tapConfig.host + $window.tapConfig.apis.movies.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset).replace('&filter[language][condition][path]=field_movie_language.uuid&filter[language][condition][value]={{language.id}}','')
                        });
                    }else{
                         return $http({
                            method: $window.tapConfig.apis.movies.list.method,
                            url: $window.tapConfig.host + $window.tapConfig.apis.movies.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset).replace('{{language.id}}',languageId)
                        });
                    }
                };
                /**
                 * [getMovieCategory description]
                 * @return {[type]} [description]
                 */
                this.getMovieCategory = function() {
                    return $http({
                        method: $window.tapConfig.apis.categories.movie_languages.method,
                        url: $window.tapConfig.host + $window.tapConfig.apis.categories.movie_languages.url
                    });

                };

                /**
                 * [getSports description]
                 * @param  {[type]} pageOffset [description]
                 * @param  {[type]} sportsId   [description]
                 * @return {[type]}            [description]
                 */
                this.getSports = function(pageOffset,sportsId) {
                    if(sportsId === 'all'){
                        return $http({
                            method: $window.tapConfig.apis.sports.list.method,
                            url: $window.tapConfig.host + $window.tapConfig.apis.sports.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset).replace('&filter[sports][condition][path]=field_sports_category.uuid&filter[sports][condition][value]={{sports.id}}','')
                        });
                    }else{
                        return $http({
                            method: $window.tapConfig.apis.sports.list.method,
                            url: $window.tapConfig.host + $window.tapConfig.apis.sports.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset).replace('{{sports.id}}',sportsId)
                        });
                    }
                };

                /**
                 * [getSportsCategory description]
                 * @return {[type]} [description]
                 */
                this.getSportsCategory = function() {
                    return $http({
                        method: $window.tapConfig.apis.categories.sports_categories.method,
                        url: $window.tapConfig.host + $window.tapConfig.apis.categories.sports_categories.url
                    });
                };

                /**
                 * [getAwards description]
                 * @param  {[type]} pageOffset [description]
                 * @return {[type]}            [description]
                 */
                this.getAwards = function(pageOffset) {
                    return $http({
                        method: $window.tapConfig.apis.awards.list.method,
                          url: $window.tapConfig.host + $window.tapConfig.apis.awards.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}', pageOffset)
                    });
                };
                this.getawardDetail = function(awardid) {
                    return $http({
                     method: $window.tapConfig.apis.awards.item.method,
                        url: $window.tapConfig.host + $window.tapConfig.apis.awards.item.url.replace('{{id}}', awardid)
                    });
                };

                /**
                 * [getFollowing description]
                 * @param  {[type]} userid       [description]
                 * @param  {[type]} pageOffset   [description]
                 * @param  {[type]} search_query [description]
                 * @return {[type]}              [description]
                 */
                this.getFollowing = function(userid, pageOffset,search_query) {
                    if(search_query === 'all'){
                        return $http({
                            method: $window.tapConfig.apis.follow.following.method,
                            url: $window.tapConfig.host + $window.tapConfig.apis.follow.following.url.replace('{{user_id}}', userid).replace('{{page.limit}}','10').replace('{{page.offset}}', pageOffset).replace('{{search_query}}','')
                        });
                    }else{
                        return $http({
                            method: $window.tapConfig.apis.follow.following.method,
                            url: $window.tapConfig.host + $window.tapConfig.apis.follow.following.url.replace('{{user_id}}', userid).replace('{{page.limit}}','10').replace('{{page.offset}}', pageOffset).replace('{{search_query}}',search_query)
                        });
                    }
                };

                /**
                 * [getFollowers description]
                 * @param  {[type]} userid       [description]
                 * @param  {[type]} pageOffset   [description]
                 * @param  {[type]} search_query [description]
                 * @return {[type]}              [description]
                 */
                this.getFollowers = function(userid, pageOffset,search_query) {
                    if(search_query === 'all'){
                        return $http({
                            method: $window.tapConfig.apis.follow.followers.method,
                            url: $window.tapConfig.host + $window.tapConfig.apis.follow.followers.url.replace('{{user_id}}', userid).replace('{{page.limit}}','10').replace('{{page.offset}}', pageOffset).replace('{{search_query}}','')
                        });
                    }else{
                        return $http({
                           method: $window.tapConfig.apis.follow.followers.method,
                            url: $window.tapConfig.host + $window.tapConfig.apis.follow.followers.url.replace('{{user_id}}', userid).replace('{{page.limit}}','10').replace('{{page.offset}}', pageOffset).replace('{{search_query}}',search_query)
                        });
                    }
                };


                /**
                 * [getReportCategory description]
                 * @return {[type]} [description]
                 */
                this.getReportCategory = function(){
                     return $http({
                        method: $window.tapConfig.apis.categories.report_category.method,
                        url: $window.tapConfig.host + $window.tapConfig.apis.categories.report_category.url
                      });
                }

                /**
                 * [postPoll description]
                 * @param  {[type]}   getpollId [description]
                 * @param  {[type]}   pollchid  [description]
                 * @param  {Function} callback  [description]
                 * @return {[type]}             [description]
                 */
                this.postPoll = function(getpollId,pollchid,callback) {
                    CSRF.getToken()
                        .then(function(response) {
                        $http({
                            method: $window.tapConfig.apis.poll.save.method,
                            url: $window.tapConfig.host + $window.tapConfig.apis.poll.save.url.replace('{{poll_id}}', getpollId),
                            data: { chid:pollchid},
                            headers: {
                                'Content-Type': 'application/json; charset=UTF-8',
                                'X-CSRF-TOKEN': response.data.token
                            }
                        }).success(function(data){
                            callback(data);
                        }).error(function(data){
                            callback(data);
                        });
                    }).catch(function(err) {
                        $log.error(err);
                    });
                };

                /**
                 * [postmovieRating description]
                 * @param  {[type]} ratingperc [description]
                 * @param  {[type]} dataid     [description]
                 * @return {[type]}            [description]
                 */
                this.postmovieRating = function(ratingperc, dataid) {
                    return $http({
                        method: $window.tapConfig.apis.movies.rate.method,
                        url: $window.tapConfig.host + $window.tapConfig.apis.movies.rate.url.replace('{{id}}', dataid),
                        data: { vote: ratingperc}
                    });
                };

                /**
                 * [postsportRating description]
                 * @param  {[type]} ratingperc [description]
                 * @param  {[type]} dataid     [description]
                 * @return {[type]}            [description]
                 */
                this.postsportRating = function(ratingperc, dataid) {
                    return $http({
                        method: $window.tapConfig.apis.sports.rate.method,
                        url: $window.tapConfig.host + $window.tapConfig.apis.sports.rate.url.replace('{{id}}', dataid),
                        data: { vote: ratingperc}
                    });
                };

                /**
                 * [getReports description]
                 * @return {[type]} [description]
                 */
                this.getReports = function(){
                    return $http({
                        method: $window.tapConfig.apis.categories.report_category.method,
                        url: $window.tapConfig.host + $window.tapConfig.apis.categories.report_category.url
                    });
                };

                /**
                 * [ReportContent description]
                 * @param {[type]}   postReport [description]
                 * @param {Function} callback   [description]
                 */
                this.ReportContent =  function(postReport,callback) {
                     CSRF.getToken()
                        .then(function(response) {
                            $http({
                                method: $window.tapConfig.apis.report.poll.method,
                                url: $window.tapConfig.host + $window.tapConfig.apis.report.poll.url,
                                data: postReport,
                                headers: {
                                    'Content-Type': 'application/json; charset=UTF-8',
                                    'X-CSRF-TOKEN': response.data.token
                                }
                           }).success(function(data){
                            callback(data);
                        }).error(function(data){
                            callback(data);
                        });
                    }).catch(function(err) {
                        $log.error(err);
                    });
                };

                /**
                 * [setMetaTags description]
                 * @param {[type]} tagData [description]
                 */
                this.setMetaTags = function (tagData){
                      $window.document.querySelector("meta[property='og:image']").content = tagData.img;
                      $window.document.querySelector("meta[property='og:description']").content = tagData.description;
                      $window.document.querySelector("meta[property='og:title']").content = tagData.description;
                };
            }

})();
