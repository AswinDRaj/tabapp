(function() {
  'use strict';

  angular.module('tapWeb')
    .service('TimelineService', TimelineService)
  /** @ngInject */
  function TimelineService(CSRF, $log, $http, $window) {
    /**
     * [gettimelineAllItems description]
     * @param  {[type]} pageOffset [description]
     * @return {[type]}            [description]
     */
    this.gettimelineAllItems = function(pageOffset) {
      return $http({
        method: $window.tapConfig.apis.timeline.list.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.timeline.list.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset)
      });
    };
    /**
     * [gettimelineCatagory description]
     * @return {[type]} [description]
     */
    this.gettimelineCatagory = function() {
      return $http({
        method: $window.tapConfig.apis.categories.interests.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.categories.interests.url
      });
    };
    /**
     * [gettimelinemyPostItems description]
     * @param  {[type]} pageOffset [description]
     * @return {[type]}            [description]
     */
    this.gettimelinemyPostItems = function(pageOffset) {
      return $http({
        method: $window.tapConfig.apis.timeline.my_posts.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.timeline.my_posts.url.replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset)
      });
    };

    /**
     * [gettimelineshareItems description]
     * @param  {[type]} getUuid [description]
     * @return {[type]}         [description]
     */
    this.gettimelineshareItems = function (getUuid) {
      return $http({
        method: $window.tapConfig.apis.timeline.item.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.timeline.item.url.replace('{{id}}',getUuid)
      });
    };

    /**
     * [postTimeline description]
     * @param  {[type]}   postType     [description]
     * @param  {[type]}   categoryType [description]
     * @param  {[type]}   questionname [description]
     * @param  {Function} callback     [description]
     * @return {[type]}                [description]
     */
    this.postTimeline = function(postType, categoryType,questionname, callback) {
      CSRF.getToken()
        .then(function(response) {
          $http({
            method: $window.tapConfig.apis.user_poll.create.method,
            url: $window.tapConfig.host + $window.tapConfig.apis.user_poll.create.url,
            data: { question: questionname, public: postType, categories: categoryType},
            headers: {
              'Content-Type': 'application/json; charset=UTF-8',
              'X-CSRF-TOKEN': response.data.token
            }
          }).success(function(data){
              callback(data);
          }).error(function(data){
              callback(data);
          });
      }).catch(function(err) {
          $log.error(err);
      });
    };

    /**
     * [getviewcomments description]
     * @param  {[type]} getCommentId [description]
     * @param  {[type]} pageOffset   [description]
     * @return {[type]}              [description]
     */
    this.getviewcomments = function(getCommentId, pageOffset) {
      return $http({
        method: $window.tapConfig.apis.user_comment.list.method,
        url: $window.tapConfig.host + $window.tapConfig.apis.user_comment.list.url.replace('{{content_id}}',getCommentId).replace('{{page.limit}}','10').replace('{{page.offset}}',pageOffset)
      });

    };

    /**
     * [postDelete description]
     * @param  {[type]}   user_poll_id [description]
     * @param  {Function} callback     [description]
     * @return {[type]}                [description]
     */
    this.postDelete = function(user_poll_id,callback) {
       CSRF.getToken()
        .then(function(response) {
          $http({
            method: $window.tapConfig.apis.user_poll.delete.method,
             url: $window.tapConfig.host + $window.tapConfig.apis.user_poll.delete.url.replace('{{user_poll_id}}',user_poll_id),
             headers: {
              'Content-Type': 'application/json; charset=UTF-8',
              'X-CSRF-TOKEN': response.data.token
            }
           }).success(function(data){
              callback(data);
          }).error(function(data){
              callback(data);
          });
      }).catch(function(err) {
          $log.error(err);
      });
    };

    /**
     * [postviewcomments description]
     * @param  {[type]}   commentdataID [description]
     * @param  {[type]}   commentname   [description]
     * @param  {Function} callback      [description]
     * @return {[type]}                 [description]
     */
    this.postviewcomments = function(commentdataID, commentname, callback) {
      CSRF.getToken()
        .then(function(response) {
          $http({
            method: $window.tapConfig.apis.user_comment.create.method,
            url: $window.tapConfig.host + $window.tapConfig.apis.user_comment.create.url.replace('{{content_id}}',commentdataID),
            data: { comment: commentname },
            headers: {
              'Content-Type': 'application/json; charset=UTF-8',
              'X-CSRF-TOKEN': response.data.token
            }
          }).success(function(data) {
            callback(data);
          }).error(function(data) {
            callback(data);
          });
        });
    };

  }


})();
