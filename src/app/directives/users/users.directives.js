 (function() {
    'use strict';
 angular.module('tapWeb')
 .directive('sidebar', function() {
            return {
                restrict: 'E',
                templateUrl: 'app/templates/users/userpage.html',
                controller: 'UserController',
                controllerAs: 'users',
                bindToController: true
            };
        });
})();