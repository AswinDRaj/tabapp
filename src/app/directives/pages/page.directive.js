(function() {
  'use strict';

  angular
    .module('tapWeb')
    .directive('modalDialog', function() {
      return {
        restrict: 'E',
        controller: 'PageController',
        scope: {
          show: '='
        },
        replace: true, // Replace with the template below
        transclude: true, // we want to insert custom content inside the directive
        link: function(scope, element, attrs) {
          scope.dialogStyle = {};
          if (attrs.width)
            scope.dialogStyle.width = attrs.width;
          if (attrs.height)
            scope.dialogStyle.height = attrs.height;

        },
        template: '<div class="ng-modal-dialog" ng-style="dialogStyle"><div class="ng-modal-dialog-content" ng-transclude></div></div>'
      };
    });

})();
