(function() {
  'use strict';
  angular
    .module('tapWeb')
    .directive("report", function($window, PageAuthService) {
      var reportdirective = {};
      reportdirective.scope = {
            report: '=data'
      },
      reportdirective.template = '<div class="dropdown"><a class="dropdownui">' +
        '<i class="fa fa-angle-down" aria-hidden="true"></i></a><ul class="dropLists">' +
        '<li class="" ng-repeat="getData in report" ng-click="clickData(getData.uuid)">' +
        '<input type="checkbox"/><label>{{getData.name}}</label>' +
        '</li></ul></div>';

      reportdirective.link = function(scope, elements, attr) {
        scope.clickData = function(getid) {
          var postReport = { content: attr.dataid, category: getid, type: attr.datatype };
          PageAuthService.ReportContent(postReport, function(response) {
            scope.getRes = response.data;
          });
        };
      };
      return reportdirective;

    });
})();
