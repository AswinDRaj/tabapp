(function() {
    'use strict';

    angular
        .module('tapWeb')
        .directive("rating", function($log, PageAuthService) {
            var directive = {};
            directive.restrict = 'AE';

            directive.scope = {
                score: '=score',
                max: '=max'
            };
            directive.replace= true;
            directive.template = "<div class='rating'>"+
  "<a ng-repeat='star in stars' ng-mouseover='hover($index)' ng-mouseleave='stopHover()' ng-class='starColor($index)' ng-click='setRating($index)'>"+
    "<i class='fa' ng-class='starClass(star, $index)'></i>"+
  "</a></div>";

            directive.link = function(scope, elements, attr) {

                scope.updateStars = function() {
                    var idx = 0;
                    scope.stars = [];
                    for (idx = 0; idx < scope.max; idx += 1) {
                        scope.stars.push({
                            full: attr.currate > idx
                        });
                    }
                };
                if (attr.currate === "" || attr.currate === "undefined" || attr.currate === "NaN") {
                    scope.hover = function( /** Integer */ idx) {
                        scope.hoverIdx = idx;
                    };

                scope.stopHover = function() {
                        scope.hoverIdx = -1;
                 };

                scope.starColor = function( /** Integer */ idx) {
                    var starClass = 'rating-normal';
                    if (idx <= scope.hoverIdx) {
                        starClass = 'rating-highlight';
                    }
                    return starClass;
                };
                }
                scope.starClass = function( /** Star */ star, /** Integer */ idx) {
                    var starClass = 'fa-star-o';
                    if (star.full || idx <= scope.hoverIdx) {
                        starClass = 'fa-star';
                    }
                    return starClass;
                };
                 scope.setRating = function(idx) {
                    if (attr.currate === "" || attr.currate === "undefined" || attr.currate === "NaN") {
                        scope.score = idx + 1;
                        var getIndex = idx + 1;
                        var perc = (getIndex * 100) / 10;
                        var getData = angular.fromJson(attr.loopdata);
                         if (angular.isDefined(getData)) {
                             var getDataId = getData.attributes.uuid;
                            PageAuthService.postRating(perc, getDataId).then(function(response) {
                                 getData.myvote  = parseInt(response.data.rating.vote_average)/10;
                                 getData.user_voted = true;
                                 $log.log(getData);
                            });

                         }
                        scope.stopHover();
                    }else{
                        alert("already rated");
                    }
                    };

                scope.$watch('score', function(newValue) {
                    if (newValue != "null" && newValue != "undefined") {
                        scope.updateStars();

                    }
                });
            };

            return directive;
        });
})();
