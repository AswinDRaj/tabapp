(function() {
  'use strict';

  angular
    .module('tapWeb')
    .directive('acmeFooter', function() {
      return {
        restrict: 'E',
        templateUrl: 'app/components/footer/footer.html',
        controller: 'FooterController',
        controllerAs: 'vm',
        bindToController: true
      };
    })
    .controller('FooterController', function($q, $rootScope, UserService) {
      var vm = this;

      vm.hidefooterBar = false;
      vm.signLink = false;

      UserService.checkLoginStatus().then(function(response) {
        var deferred = $q.defer();
        if (response.data === '1') {
          vm.hidefooterBar = true;
          deferred.resolve();
        } else {
          vm.hidefooterBar = false;
          deferred.reject();
        }
      });
    });



})();
