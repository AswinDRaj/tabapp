(function() {
  'use strict';
  angular.module('tapWeb')
    .directive('acmeNavbar', function () {
      return{
        restrict: 'E',
        templateUrl: 'app/components/navbar/navbar.html',
        controller: 'NavbarController',
        controllerAs: 'vm',
        bindToController: true
      };
  })
   .controller('NavbarController', function($q, $window,$rootScope, UserService) {
      var events = {};
      var vm = this;
      vm.hideNavBar = false;
      vm.signLink = false;
      vm.search = null;
      vm.Response = false;

      UserService.checkLoginStatus().then(function(response) {
           var deferred = $q.defer();
           if(response.data === '1') {
                vm.hideNavBar = true;
                  deferred.resolve();
           }else{
                 vm.hideNavBar = false;
                 deferred.reject();
           }
      });
      vm.tapUserSearch = function() {
        fetch();
      }
      events.USER_LOGGED_IN =  $rootScope.$on('USER_LOGGED_IN', function() {
        vm.hideNavBar = true;
      });

      events.USER_LOGGED_OUT = $rootScope.$on('USER_LOGGED_OUT', function() {
        vm.hideNavBar = false;
      });

      function fetch(){
         UserService.userSearch(vm.search).then(function(response){
          vm.details = response.data;
          if(vm.details.data.length > 0){
            vm.Response = true;
          }else{
            vm.Response = false;
          }
        });
      }
    });


})();
