(function() {
  'use strict';

  angular
    .module('tapWeb')
    .directive('sideBar', function() {
      return{
        restrict: 'E',
        templateUrl: 'app/components/sidebar/sidebar.html',
        controller: 'UserController',
        controllerAs: 'users',
        bindToController: true
      };
    });
       
})();
