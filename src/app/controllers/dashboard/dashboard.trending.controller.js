(function () {
  'use strict';

  angular
    .module('tapWeb')
    .controller('DashboardTrendingController', DashboardTrendingController)

    /** @ngInject */
    function DashboardTrendingController(dashtrendingItemsResponse, $stateParams) {
      var vm = this;
      vm.listData = dashtrendingItemsResponse.data.data;
      vm.nextPage = dashtrendingItemsResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = dashtrendingItemsResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.dataLength = dashtrendingItemsResponse.data.data.length;
      vm.getPrev = dashtrendingItemsResponse.data.links.prev;
      vm.getNext = dashtrendingItemsResponse.data.links.next;
      angular.forEach(vm.listData, function(trendDashData) {
          angular.forEach(trendDashData.attributes.field_polls, function(dashDatatrend) {
            dashDatatrend.sum = 0;
            dashDatatrend.voted = dashDatatrend.user_voted;
            for (var i = 0; i < dashDatatrend.votes.length; i++) {
              dashDatatrend.sum = dashDatatrend.sum + parseInt(dashDatatrend.votes[i].value);
            }
          return dashDatatrend;
        });
      });
    }
})();
