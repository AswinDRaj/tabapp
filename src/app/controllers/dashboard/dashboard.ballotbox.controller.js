(function () {
  'use strict';

  angular
    .module('tapWeb')
    .controller('DashboardBallotBoxController', DashboardBallotBoxController)

    /** @ngInject */
    function DashboardBallotBoxController(dashballotBoxItemsResponse, PageAuthService, $stateParams,$scope,$log) {
      var vm = this;
    vm.options = {
        chart: {
          type: 'pieChart',
          height: 400,
          x: function(d) { return d.key; },
          y: function(d) { return d.y; },
          showLabels: false,
          legendPosition: 'right',
          duration: 500,
          labelThreshold: 0.01,
          labelSunbeamLayout: true,
          legend: {
            margin: {
              top: 5,
              right: 35,
              bottom: 5,
              left: 0
            }
          }
        }
      };

      vm.dataChart = function(getCount) {
          getCount.view = true;
          var getData = [];
          var getkey = [];
          var gety = [];
          var key, y;

          angular.forEach(getCount.choices, function(data) {
            key = data.value;
            getkey.push(key);
          });
          angular.forEach(getCount.votes, function(data) {
            y = data.value;
            gety.push(y);
          });

          for (var i = 0; i <= getCount.votes.length - 1; i++) {
            var dd = {
              key: getkey[i],
              y: gety[i]
            };
            getData.push(dd);
          }
          getCount.chart = getData;
          // nv.addGraph(function() {
          //     var positionX = 0;
          //     var positionY = 30;
          //     var verticalOffset = 25;
          //     d3.selectAll('.nv-legend .nv-series')[0].forEach(function(d) {
          //       positionY += verticalOffset;
          //       d3.select(d).attr('transform', 'translate(' + positionX + ',' + positionY + ')');
          //     });
          // });
      };
      vm.listData = dashballotBoxItemsResponse.data.data;
      vm.nextPage = dashballotBoxItemsResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = dashballotBoxItemsResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.getPrev = dashballotBoxItemsResponse.data.links.prev;
      vm.getNext = dashballotBoxItemsResponse.data.links.next;
      $log.log(vm.listData);
    angular.forEach(vm.listData, function(dashballotBoxdata) {
         dashballotBoxdata.sum = 0;
         if(angular.isDefined(dashballotBoxdata.attributes.field_polls[0])){
            dashballotBoxdata.voted = dashballotBoxdata.attributes.field_polls[0].user_voted;
            if(dashballotBoxdata.attributes.field_polls[0].user_vote_value){
            dashballotBoxdata.chid = parseInt(dashballotBoxdata.attributes.field_polls[0].user_vote_value.chid);
          }else{
            dashballotBoxdata.chid = 0;
          }
          vm.dataChart(dashballotBoxdata.attributes.field_polls[0]);
          for (var i = 0; i < dashballotBoxdata.attributes.field_polls[0].votes.length; i++) {
               dashballotBoxdata.sum = dashballotBoxdata.sum + parseInt(dashballotBoxdata.attributes.field_polls[0].votes[i].value);
          }
        }else{
          dashballotBoxdata.voted = false;
          dashballotBoxdata.chid  =0;
        }
        return dashballotBoxdata;
      });
    }
})();
