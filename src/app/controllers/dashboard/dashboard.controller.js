(function () {
  'use strict';

  angular
    .module('tapWeb')
    .controller('DashboardController', function ($state) {
      //var vm = this;

      // vm.totalUser = function (getcount) {
      //   getcount.sum = 0;
      //   for (var i = 0; i < getcount.votes.length; i++) {
      //     getcount.sum = getcount.sum + parseInt(getcount.votes[i].value);
      //   }
      //   return getcount.sum;
      // };

      if($state.current.name === 'landing.dashboard') {
        $state.go('landing.dashboard.trending');
      }
    });
})();
