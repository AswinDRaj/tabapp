(function() {
'use strict';

angular
  .module('tapWeb')
  .controller('TimelineAllController', function(timelineAllItemsResponse, $stateParams,$log,PageAuthService,TimelineService,$window) {
      var vm = this;
      vm.listData = timelineAllItemsResponse.data.data;
      vm.nextPage = timelineAllItemsResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = timelineAllItemsResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.dataLength = timelineAllItemsResponse.data.data.length;
      angular.forEach(vm.listData, function(timelineData) {
        timelineData.sum = 0;
        timelineData.voted = timelineData.attributes.field_polls[0].user_voted;
        if(timelineData.attributes.field_polls[0].user_vote_value){
          timelineData.chid = parseInt(timelineData.attributes.field_polls[0].user_vote_value.chid);
        }else{
           timelineData.chid = 0;
        }
        for (var i = 0; i < timelineData.attributes.field_polls[0].votes.length; i++) {
          timelineData.sum = timelineData.sum + parseInt(timelineData.attributes.field_polls[0].votes[i].value);
        }
        return timelineData;
      });
      angular.forEach(vm.listData,function(getComment){
          vm.gettimeshareId = getComment.attributes.uuid;
          getComment.shareurl = $window.tapConfig.share_links.user_post.url.replace('{uuid}',vm.gettimeshareId);
          var pageComOffset = 0;
        TimelineService.getviewcomments(getComment.attributes.uuid,pageComOffset).then(function(response) {
          vm.commentsdata = response.data.data;
          getComment.view = false;
          if (getComment.poststrigger) {
              getComment.view = true;
          }
          getComment.Count = parseInt(getComment.attributes.field_comments_count);
          getComment.commentData = response.data.data;

        });
        return getComment;
      });
      vm.PostComments = function(allTimeline, commentname) {
        var commentdataID = allTimeline.attributes.uuid;
        TimelineService.postviewcomments(commentdataID, commentname,function(response) {
      allTimeline.commentData.splice(0,0,response);
      allTimeline.attributes.field_comments_count = parseInt(allTimeline.attributes.field_comments_count) + 1;
        });
      };
      vm.selectPoll = function(timeLinePoll, getVote) {
          var getPollId = timeLinePoll.attributes.field_polls[0].id;
          var pollchid =  getVote.id;
          PageAuthService.postPoll(getPollId, pollchid, function(response) {
          if (response.message) {
            alert(response.message);
          }else {
            vm.getpoll = response.status;
              timeLinePoll.voted = true;
              timeLinePoll.sum = parseInt(timeLinePoll.sum)+1;
              timeLinePoll.chid = pollchid;

          }
        });
      };
      vm.ClickComments = function(item) {
        if (item.view) {
          item.view = false;
        } else {
          item.view = true;
        }
      };
    });

  })();
