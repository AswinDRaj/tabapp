(function() {
  'use strict';

  angular
    .module('tapWeb')
    .controller('TimelinemyController', function($rootScope, TimelineService, timelinemyPostItemsResponse, $stateParams, $log, $window) {
      var vm = this;
      $rootScope.cmtname;
      vm.listData = timelinemyPostItemsResponse.data.data;
      vm.nextPage = timelinemyPostItemsResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = timelinemyPostItemsResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.dataLength = timelinemyPostItemsResponse.data.data.length;
      vm.getPrev = timelinemyPostItemsResponse.data.links.prev;
      vm.getNext = timelinemyPostItemsResponse.data.links.next;
      angular.forEach(vm.listData, function(getTimeline) {

        getTimeline.sum = 0;
        for (var i = 0; i < getTimeline.attributes.field_polls[0].votes.length; i++) {
          getTimeline.sum = getTimeline.sum + parseInt(getTimeline.attributes.field_polls[0].votes[i].value);
        }
        getTimeline.voted = getTimeline.attributes.field_polls[0].user_voted;

        if(getTimeline.attributes.field_polls[0].user_vote_value) {
          getTimeline.chid = parseInt(getTimeline.attributes.field_polls[0].user_vote_value.chid);
        } else {
          getTimeline.chid = 0;
        }

        /**
         * Get All Comments from each section
         */

        var pageComOffset = 0;
        vm.gettimeshareId = getTimeline.attributes.uuid;
        getTimeline.shareurl = $window.tapConfig.share_links.user_post.url.replace('{uuid}',vm.gettimeshareId);
        TimelineService.getviewcomments(getTimeline.attributes.uuid,pageComOffset).then(function(response) {
          vm.commentsdata = response.data.data;
            getTimeline.view = false;
            if (getTimeline.poststrigger) {
              getTimeline.view = true;
            }
            getTimeline.Count = parseInt(getTimeline.attributes.field_comments_count);
            getTimeline.commentData = response.data.data;
          });


        return getTimeline; // Return Updated Data
      });

      vm.PostComments = function(mydashboardtime, commentname) {
        var commentdataID = mydashboardtime.attributes.uuid;
        TimelineService.postviewcomments(commentdataID, commentname, function(response) {
          mydashboardtime.commentData.splice(0, 0, response);
          mydashboardtime.Count = mydashboardtime.Count + 1;
        });
      };

      vm.ClickComments = function(item) {
        if (item.view) {
          item.view = false;
        } else {
          item.view = true;
        }
      };
      vm.deletePost = function(post){
          TimelineService.postDelete(post.attributes.uuid,function(response){
              alert(response.message);
              var getIndex = vm.listData.indexOf(post);
              vm.listData.splice(getIndex, 1);
         });
      }
    });

})();
