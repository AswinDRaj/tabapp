(function() {
'use strict';

angular
  .module('tapWeb')
  .controller('timelineShareController', function(timelineShareItemsResponse) {
      var vm = this;
      vm.timeSum = 0;
      vm.timelineShare = timelineShareItemsResponse.data.data;
      vm.timelinevotes = timelineShareItemsResponse.data.data.attributes.field_polls[0];
      for (var i = 0; i < vm.timelinevotes.votes.length; i++) {
               vm.timeSum = vm.timeSum + parseInt(vm.timelinevotes.votes[i].value);

        }

    });

  })();
