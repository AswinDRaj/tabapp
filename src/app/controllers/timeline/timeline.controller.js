(function() {
  'use strict';
  angular
    .module('tapWeb')
    .controller('TimelineController', function(timelinecategoryResponse, TimelineService, $window, $rootScope, $scope, $stateParams, $log, $state) {
      var vm = this;
      vm.gettimeCategory = timelinecategoryResponse.data;
      vm.toggleModal = function() {
        vm.modalShown = true;
      };
      vm.hideModal = function() {
        vm.modalShown = false;
      };
      vm.goBack = function() {
        $window.history.back();
      };
      vm.postTime = function(postType, categoryType, questionname) {
        var getBoolean = false;
        if (postType === 'true') {
          getBoolean = true;
        } else {
          getBoolean = false;
        }
        TimelineService.postTimeline(getBoolean, categoryType, questionname, function(response) {
          if (response.id) {
            vm.gettime = response;
            if (response.attributes.field_public) {
              alert("Public Post has been posted");

            } else {
              alert("Private Post has been posted");

            }
            vm.hideModal();
          }
        });
      };
      if ($state.current.name === 'landing.timeline') {
        $state.go('landing.timeline.all');
      }
    });

})();
