(function() {
'use strict';

angular
  .module('tapWeb')
  .controller('TrendingController', function(trendingItemsResponse, PageAuthService,$rootScope, $stateParams) {
      var vm = this;
      vm.listData = trendingItemsResponse.data.data;
      vm.nextPage = trendingItemsResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = trendingItemsResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.dataLength = trendingItemsResponse.data.data.length;
      vm.getPrev = trendingItemsResponse.data.links.prev;
      vm.getNext = trendingItemsResponse.data.links.next;
      PageAuthService.getReports().then(function(response) {
            vm.report = response.data;
      });
    });
  })();
