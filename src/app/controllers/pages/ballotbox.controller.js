(function() {
  'use strict';

  angular
    .module('tapWeb')
    .controller('BallotboxController', function(ballotBoxItemsResponse, PageAuthService, $stateParams, $window) {
      var vm = this;
      vm.options = {
        chart: {
          type: 'pieChart',
          height: 400,
          x: function(d) { return d.key; },
          y: function(d) { return d.y; },
          showLabels: false,
          legendPosition: 'right',
          duration: 500,
          labelThreshold: 0.01,
          labelSunbeamLayout: true,
          legend: {
            margin: {
              top: 5,
              right: 35,
              bottom: 5,
              left: 0
            }
          }
        }
      };

      vm.dataChart = function(getCount) {
        getCount.view = true;
        var getData = [];
        var getkey = [];
        var gety = [];
        var key, y;

        angular.forEach(getCount.choices, function(data) {
          key = data.value;
          getkey.push(key);
        });
        angular.forEach(getCount.votes, function(data) {
          y = data.value;
          gety.push(y);
        });

        for (var i = 0; i <= getCount.votes.length - 1; i++) {
          var dd = {
            key: getkey[i],
            y: gety[i]
          };
          getData.push(dd);
        }
        getCount.chart = getData;
        // nv.addGraph(function() {
        //     var positionX = -30;
        //     var positionY = 30;
        //     var verticalOffset = 25;
        //     d3.selectAll('.nv-legend .nv-series')[0].forEach(function(d) {
        //       positionY += verticalOffset;
        //       d3.select(d).attr('transform', 'translate(' + positionX + ',' + positionY + ')');
        //     });
        // });
      };
      vm.listData = ballotBoxItemsResponse.data.data;
      vm.nextPage = ballotBoxItemsResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = ballotBoxItemsResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.dataLength = ballotBoxItemsResponse.data.data.length;
      vm.getPrev = ballotBoxItemsResponse.data.links.prev;
      vm.getNext = ballotBoxItemsResponse.data.links.next;
      vm.totalData = ballotBoxItemsResponse.data.page.total;

      angular.forEach(vm.listData, function(ballotBoxdata) {

        ballotBoxdata.sum = 0;
        ballotBoxdata.voted = ballotBoxdata.attributes.field_polls[0].user_voted;
        if (ballotBoxdata.attributes.field_polls[0].user_vote_value) {
          vm.shareid = ballotBoxdata.attributes.uuid;
          ballotBoxdata.chid = parseInt(ballotBoxdata.attributes.field_polls[0].user_vote_value.chid);
        }else {
          ballotBoxdata.chid = 0;
        }
        ballotBoxdata.shareurl = $window.tapConfig.share_links.ballot_box.url.replace('{uuid}', vm.shareid);
        for (var i = 0; i < ballotBoxdata.attributes.field_polls[0].votes.length; i++) {
          ballotBoxdata.sum = ballotBoxdata.sum + parseInt(ballotBoxdata.attributes.field_polls[0].votes[i].value);
        }
        vm.dataChart(ballotBoxdata.attributes.field_polls[0]);
        return ballotBoxdata;
      });
       PageAuthService.getReports().then(function(response) {
            vm.report = response.data;
      });
      vm.selectPoll = function(ballotBoxdata, getVote) {
        var getPollId = ballotBoxdata.attributes.field_polls[0].id;
        var pollchid = getVote.id;
        PageAuthService.postPoll(getPollId, pollchid, function(response) {
          if (response.message) {
            alert(response.message);
          } else {
            vm.getpoll = response.status;
            if (vm.getpoll === '1') {
              ballotBoxdata.voted = true;
              ballotBoxdata.sum = parseInt(ballotBoxdata.sum) + 1;
              ballotBoxdata.chid = pollchid;
            }
          }
        });
      };
    })
    .filter('roundup', function() {
      return function(value) {
        return Math.ceil(value);
      };
    });

})();
