(function() {
'use strict';

angular
  .module('tapWeb')
  .controller('ballotShareController', function(ballotShareItemsResponse,$log,PageAuthService) {
      var vm = this;
      vm.ballotShare = ballotShareItemsResponse.data.data;
      vm.ballotvotes = ballotShareItemsResponse.data.data.attributes.field_polls[0];
      vm.ballotSum = 0;
      vm.options = {
        chart: {
          type: 'pieChart',
          height: 400,
          x: function(d) { return d.key; },
          y: function(d) { return d.y; },
          showLabels: false,
          legendPosition: 'right',
          duration: 500,
          labelThreshold: 0.01,
          labelSunbeamLayout: true,
          legend: {
            margin: {
              top: 0,
              right: 35,
              bottom: 5,
              left: 10
            }
          }
        }
      };
      vm.dataChart = function(getCount) {

          var getData = [];
          var getkey = [];
          var gety = [];
          var key, y;

          angular.forEach(getCount.choices, function(data) {
            key = data.value;
            getkey.push(key);
          });
          angular.forEach(getCount.votes, function(data) {
            y = data.value;
            gety.push(y);
          });

          for (var i = 0; i <= getCount.votes.length - 1; i++) {
            var dd = {
              key: getkey[i],
              y: gety[i]
            };
            getData.push(dd);
          }
          getCount.chart = getData;
          // nv.addGraph(function() {
          //     var positionX = 0;
          //     var positionY = 30;
          //     var verticalOffset = 25;
          //     d3.selectAll('.nv-legend .nv-series')[0].forEach(function(d) {
          //       positionY += verticalOffset;
          //       d3.select(d).attr('transform', 'translate(' + positionX + ',' + positionY + ')');
          //     });
          // });
      };
       for (var i = 0; i < vm.ballotvotes.votes.length; i++) {
               vm.ballotSum = vm.ballotSum + parseInt(vm.ballotvotes.votes[i].value);
        }
        vm.dataChart(vm.ballotvotes);
        var subTitle = vm.ballotvotes.question;
            PageAuthService.setMetaTags({
              img: '',
              description: subTitle
            });
    });
  })();
