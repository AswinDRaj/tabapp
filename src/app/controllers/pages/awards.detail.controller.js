(function() {
  'use strict';

  angular
    .module('tapWeb')
    .controller('AwarddetailController', function(PageAuthService, awardsdetailsItemsResponse, $stateParams, $log, $window) {
      var vm = this;
      var height = $window.innerHeight / 2;
      vm.options = {
        chart: {
          type: 'pieChart',
          height: height,
          x: function(d) { return d.key; },
          y: function(d) { return d.y; },
          showLabels: false,
          legendPosition: 'right',
          duration: 500,
          labelThreshold: 0.01,
          labelSunbeamLayout: true,
          legend: {
            margin: {
              top: 5,
              right: 35,
              bottom: 5,
              left: 0
            }
          }
        }
      };
      vm.dataChart = function(getCount) {
        getCount.view = true;
        var getData = [];
        var getkey = [];
        var gety = [];
        var key, y;

        angular.forEach(getCount.choices, function(data) {
          key = data.value;
          getkey.push(key);
        });
        angular.forEach(getCount.votes, function(data) {
          y = data.value;
          gety.push(y);
        });

        for (var i = 0; i <= getCount.votes.length - 1; i++) {
          var dd = {
            key: getkey[i],
            y: gety[i]
          };
          getData.push(dd);
        }
        getCount.chart = getData;

      };
      vm.listData = awardsdetailsItemsResponse.data.data;
      vm.awarddata = vm.listData.attributes.field_polls;
      angular.forEach(vm.awarddata, function(awardDetailData) {
        awardDetailData.sum = 0;
        awardDetailData.voted = awardDetailData.user_voted;
        if (awardDetailData.user_vote_value) {
          awardDetailData.chid = parseInt(awardDetailData.user_vote_value.chid);
        } else {
          awardDetailData.chid = 0;
        }
        vm.dataChart(awardDetailData);
        for (var i = 0; i < awardDetailData.votes.length; i++) {
          awardDetailData.sum = awardDetailData.sum + parseInt(awardDetailData.votes[i].value);
        }
        return awardDetailData;
      });
      PageAuthService.getReports().then(function(response) {
            vm.report = response.data;
      });


      vm.selectPoll = function(awardDetailData, getVote) {
        var getPollId = awardDetailData.id;
        var pollchid = getVote.id;
        PageAuthService.postPoll(getPollId, pollchid, function(response) {
          if (response.message) {
            alert(response.message);
          } else {
            vm.getpoll = response.status;
            if (vm.getpoll === '1') {
              awardDetailData.voted = true;
              awardDetailData.sum = parseInt(awardDetailData.sum) + 1;
              awardDetailData.chid = pollchid;
            }
          }
        });
      };
    })
    .filter('roundup', function() {
      return function(value) {
        return Math.ceil(value);
      };
    });
})();
