(function() {
'use strict';

angular
  .module('tapWeb')
  .controller('AwardController', function(PageAuthService,awardsItemsResponse, $stateParams) {
      var vm = this;
      vm.listData = awardsItemsResponse.data.data;
      vm.nextPage = awardsItemsResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = awardsItemsResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.dataLength = awardsItemsResponse.data.data.length;
      vm.getPrev = awardsItemsResponse.data.links.prev;
      vm.getNext = awardsItemsResponse.data.links.next;
      angular.forEach(vm.listData,function(awardresponse){
        PageAuthService.getawardDetail(awardresponse.attributes.uuid).then(function(response) {
          vm.awarddata = response.data.data;
          vm.awardsdata = response.data.data.attributes.field_polls;
          vm.getCurrentid = vm.awardsdata.choices;
        });

      });
    });
  })();
