(function() {
'use strict';

angular
  .module('tapWeb')
  .controller('SportsController', function(sportsItemsResponse, $stateParams,PageAuthService,$log,$state) {
      var vm = this;
      vm.listData = sportsItemsResponse.data.data;
      vm.nextPage = sportsItemsResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = sportsItemsResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.dataLength = sportsItemsResponse.data.data.length;
      vm.getPrev = sportsItemsResponse.data.links.prev;
      vm.getNext = sportsItemsResponse.data.links.next;
      vm.selectedCateg = $stateParams.sportId;
      vm.starLength = 10;
      vm.starsArray = [];
      for(var i = 0;i<= vm.starLength-1;i++){
        var star = {'star': 'star'+i}
        vm.starsArray.push(star);
      }
      PageAuthService.getSportsCategory().then(function(response) {
          vm.getCategory = response.data;
      });
      angular.forEach(vm.listData, function(sportItemData) {
              sportItemData.tapVote = parseInt((sportItemData.attributes.rating.vote_average/10).toFixed(0));
           if(sportItemData.attributes.rating.user_vote){
               sportItemData.myvote = parseInt(sportItemData.attributes.rating.user_vote/10);
               sportItemData.user_voted = true;
           }
           else{
            sportItemData.myvote = 0;
            sportItemData.user_voted = false;
           }
            sportItemData.stars = vm.starsArray;
           return sportItemData;
       });
        vm.ratingStar = function(data,index){
          var perc = parseInt(index) + 1;
          var getPerc = perc *10;
          if (data.user_voted) {
            alert("already Voted");
          }else{
              PageAuthService.postsportRating(getPerc, data.id).then(function(response) {
                    data.tapVote = parseInt((response.data.rating.vote_average/10).toFixed(0));
                    data.myvote  = perc;
                    data.user_voted = true;
                   $log.log(response);
             });
          }
        };
         vm.sportCateg = function(sportId){
          $state.go('landing.sports',{ 'sportId': sportId});
        };
    });

  })();
