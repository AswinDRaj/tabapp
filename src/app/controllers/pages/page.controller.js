(function() {
  'use strict';

  angular
    .module('tapWeb')
    .controller('PageController', function() {
      var vm = this;
        vm.tab = 1;
        vm.setTabPanel = function(newTab){
              vm.tab = newTab;
        };

        vm.isSetPanel = function(tabNum){
              return vm.tab === tabNum;
        };
    })
    .filter('roundup', function() {
      return function(value) {
        return Math.ceil(value);
      };
    });
})();
