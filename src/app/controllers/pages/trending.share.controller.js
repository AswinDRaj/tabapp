(function() {
  'use strict';

  angular
    .module('tapWeb')
    .controller('TrendingShareController', function(trendingShareItemsResponse, $log,PageAuthService,$rootScope, $stateParams) {
      var vm = this;
      vm.shareTrend = trendingShareItemsResponse.data.data;
      vm.topicShare = trendingShareItemsResponse.data.data.attributes.field_polls;
      var shareimg = vm.shareTrend.attributes.images.large;
      vm.sum = 0;
      if ($stateParams.pollid) {
        angular.forEach(vm.topicShare, function(data) {
          data.sum = 0;
          if (data.id === $stateParams.pollid) {
            vm.partiTopic = data;
            var subTitle = vm.partiTopic.question;
            PageAuthService.setMetaTags({
              img: shareimg,
              description: subTitle
            });

          }
          return vm.partiTopic;
        });
      }
      for (var i = 0; i < vm.partiTopic.votes.length; i++) {
        vm.sum = vm.sum + parseInt(vm.partiTopic.votes[i].value);
      }
    });
})();
