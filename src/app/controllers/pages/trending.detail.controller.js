(function() {
'use strict';

angular
  .module('tapWeb')
  .controller('TrendingDetailController', function(trendingDetailItemsResponse,PageAuthService,$log,$stateParams,$window) {
      var vm = this;
      vm.extractData = trendingDetailItemsResponse.data.data;
      vm.loopData = trendingDetailItemsResponse.data.data.attributes.field_polls;
      vm.getCurrentid = vm.loopData.choices;
      vm.getshareId = $stateParams.trendingId;
      angular.forEach(vm.loopData, function(trendDetailData) {
        trendDetailData.sum = 0;
        trendDetailData.voted = trendDetailData.user_voted;
        if(trendDetailData.user_vote_value){
          trendDetailData.chid = parseInt(trendDetailData.user_vote_value.chid);
          trendDetailData.shareurl = $window.tapConfig.share_links.trending.url.replace('{uuid}',vm.getshareId).replace('{poll_id}',trendDetailData.id);
        }else{
           trendDetailData.chid = 0;
           trendDetailData.shareurl = '';

        }
        for (var i = 0; i < trendDetailData.votes.length; i++) {
          trendDetailData.sum = trendDetailData.sum + parseInt(trendDetailData.votes[i].value);
        }
        return trendDetailData;
      });
      PageAuthService.getReports().then(function(response) {
            vm.report = response.data;
      });
      vm.selectPoll = function(trendingDetail, getVote) {
          var getPollId = trendingDetail.id;
          var pollchid =  getVote.id;
          //$log.log(pollchid);
          PageAuthService.postPoll(getPollId, pollchid, function(response) {
          if (response.message) {
            alert(response.message);
          }else {
            vm.getpoll = response.status;
            //$log.log(response);
            if(vm.getpoll ==='1'){
              trendingDetail.voted = true;
              trendingDetail.sum = parseInt(trendingDetail.sum)+1;
              trendingDetail.chid = pollchid;
            }
          }
        });
      };

    })
      .filter('roundup', function() {
      return function(value) {
        return Math.ceil(value);
      };
    });

      // vm.trendingShare = function($stateParams) {
      //   vm.trendingtopic = $stateParams.trendingId;
      //   vm.topicData = $stateParams.topicTitle;
      //   // $log.log(vm.trendingtopic,vm.topicData);
      //   PageAuthService.trendingSingle(vm.trendingtopic, function(response) {
      //     vm.extractshareData = response.data;
      //     // $log.log(vm.extractshareData);

      //   });
      //   PageAuthService.trendingSingle(vm.topicData, function(response) {
      //     vm.shareMainData = response.data;
      //     // $log.log(vm.shareMainData);

      //   });
      // };

  })();
