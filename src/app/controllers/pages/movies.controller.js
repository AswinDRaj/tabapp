(function() {
'use strict';

angular
  .module('tapWeb')
  .controller('MovieController', function(movieItemsResponse, $stateParams,PageAuthService,$log,$state) {
      var vm = this;
      vm.listData = movieItemsResponse.data.data;
      vm.nextPage = movieItemsResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = movieItemsResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.dataLength = movieItemsResponse.data.data.length;
      vm.getPrev = movieItemsResponse.data.links.prev;
      vm.getNext = movieItemsResponse.data.links.next;
      vm.selectedLang = $stateParams.movieLanguage;
      vm.starLength = 10;
      vm.starsArray = [];
      for(var i = 0;i<= vm.starLength-1;i++){
        var star = {'star': 'star'+i}
        vm.starsArray.push(star);
      }
      PageAuthService.getMovieCategory().then(function(response) {
          vm.getCategory = response.data;
      });

      //$log.log(vm.listData);
       angular.forEach(vm.listData, function(movieItemData) {
              movieItemData.tapVote = parseInt((movieItemData.attributes.rating.vote_average/10).toFixed(0));
           if(movieItemData.attributes.rating.user_vote){
               movieItemData.myvote = parseInt(movieItemData.attributes.rating.user_vote/10);
               movieItemData.user_voted = true;
           }
           else{
            movieItemData.myvote = 0;
            movieItemData.user_voted = false;
           }
            movieItemData.stars = vm.starsArray;
           return movieItemData;
       });
       vm.ratingStar = function(data,index){
          var perc = parseInt(index) + 1;
          var getPerc = perc *10;
        if (data.user_voted) {
          alert("already Voted");
        }else{
            PageAuthService.postmovieRating(getPerc, data.id).then(function(response) {
                  data.tapVote = parseInt((response.data.rating.vote_average/10).toFixed(0));
                  data.myvote  = perc;
                  data.user_voted = true;
           });
        }
      };
      vm.language = function(langId){
        $state.go('landing.movies',{ 'movieLanguage': langId});
      }
    }).filter('roundup', function() {
      return function(value) {
        return Math.ceil(value);
      };
    });

  })();
