(function() {
  'use strict';
  angular.module('tapWeb')
    .controller('MainController', MainController)
  /** @ngInject */
  function MainController($timeout, $location, toastr, $rootScope, $window) {
    var vm = this;
    vm.android = $window.tapConfig.mobile.android.store;
    vm.ios = $window.tapConfig.mobile.ios.store;

  }

})();
