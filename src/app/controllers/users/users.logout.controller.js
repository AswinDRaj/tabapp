(function() {
  'use strict';

  angular.module('tapWeb')
  .controller('UserLogoutController', UserLogoutController);

  /** @ngInject */
  function UserLogoutController(Authenticate, $window, $location, $rootScope) {
    $rootScope.isLoggedIn = false;
    Authenticate.emptyLocalStorage();
    $window.location.href = $window.tapConfig.host + 'user/logout';
    $rootScope.$broadcast('USER_LOGGED_OUT');
  }

})();
