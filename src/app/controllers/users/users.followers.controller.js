(function () {
  'use strict';

  angular
    .module('tapWeb')
    .controller('FollowerController', function(followersItemResponse,$state,$stateParams,UserService) {
      var vm = this;
      vm.followerdata = followersItemResponse.data.data;
      vm.nextPage = followersItemResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = followersItemResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.dataLength = followersItemResponse.data.data.length;

      if($stateParams.search_query === 'all'){
         vm.userSearch = '';
      }else{
         vm.userSearch = $stateParams.search_query;
      }
      angular.forEach(vm.followerdata,function(userFollower){
          userFollower.follower  = userFollower.field_user_is_following[0].value;
      });
      vm.unfollower = function(userobject, userId) {
        UserService.unFollowPost(userId, function(response) {
          userobject.follower = response.field_user_is_following[0].value;
        });
      };
      vm.follower = function(userobject, userId) {
        UserService.unFollowPost(userId, function(response) {
           userobject.follower = response.field_user_is_following[0].value;
        });
      };
       vm.searchUser = function() {
          $state.go('landing.follow.followers',{ 'search_query': vm.userSearch});
      };
    });

})();
