(function () {
  'use strict';

  angular
    .module('tapWeb')
    .controller('FollowingController', function(followingItemResponse,$state,$stateParams,UserService) {
      var vm = this;
      vm.followingdata = followingItemResponse.data.data;
      vm.nextPage = followingItemResponse.data.links.next ? parseInt($stateParams.pageOffset) + 10 : null;
      vm.prevPage = followingItemResponse.data.links.prev ? parseInt($stateParams.pageOffset) - 10 : null;
      vm.dataLength = followingItemResponse.data.data.length;
      if($stateParams.search_query === 'all'){
         vm.followingUser = '';
      }else{
         vm.followingUser = $stateParams.search_query;
      }
      angular.forEach(vm.followingdata,function(userFollowing){
          userFollowing.following  = userFollowing.field_user_is_following[0].value;
      });

      vm.unfollowing = function(userobject, userId) {
        UserService.unFollowPost(userId, function(response) {
          userobject.following = response.field_user_is_following[0].value;
        });
      };
      vm.following = function(userobject, userId) {
        UserService.unFollowPost(userId, function(response) {
           userobject.following = response.field_user_is_following[0].value;
        });
      };
       vm.searchUser = function() {
          $state.go('landing.follow.following',{ 'search_query': vm.followingUser});
      };
    });

})();
