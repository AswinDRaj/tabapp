(function() {
  'use strict';

  angular.module('tapWeb')
    .controller('UserController', function($rootScope, $stateParams, $log, $scope,$state, $location, $timeout, $window, UserService, Authenticate) {
      var vm = this;
      vm.interests = null;
      vm.countlist = 'Need to Choose min 3';
      vm.lst = [];
      vm.isReadOnly = true;
      vm.camera = false;
      vm.modalShown = false;
      vm.countryList = $window.tapConfig.apis.countries.countries;
      vm.stateList = $window.tapConfig.apis.states.states;
      vm.shareApp   = "http://config.taponline.in/share/app";
      var getcurrentUser = Authenticate.getLocalStorage();
      vm.isSending = false;
      vm.loginErr = false;
      vm.loginSubmit = 'Sign In';
      vm.regSubmit = 'Sign Up';
      vm.forgotSubmit = 'Continue';
      vm.resetSubmit = 'Submit';
      vm.edit = true;
      vm.save = false;
      vm.availableStates = [];

      vm.hideModel = function() {
       vm.modalShown = false;
      };
      vm.showModel = function(){
         vm.modalShown = true;
      }
      /**
       * Calling Login api from UserService
       * @param name
       * @para password
       */
      vm.signin = function(name, password) {
        Authenticate.emptyLocalStorage();
        vm.loginSubmit = 'Authentication Processing...';
        UserService.Login(name, password, function(response) {
          if ($rootScope.isLoggedIn) {
            vm.loginErr = false;
            vm.isSending = false;
            vm.loginSubmit = 'Signed In';
            vm.isSending = true;

            $rootScope.$broadcast('USER_LOGGED_IN', response);
            $state.go('landing.trending');
          } else {
            vm.loginErr = true;
            vm.isSending = false;
            vm.loginErrText = response.message;
            vm.loginSubmit = 'Login';
          }
        });
      };

      /**
       * Calling SignUp API from UseService
       * @param {mail} [email] [description]
       * @param {field_full_name} [fullname] [description]
       * @param {field_date_of_birth} [dob] [description]
       * @param {field_country} [country] [description]
       * @param {field_state} [state] [description]
       * @param {pass} [password] [description]
       */
      vm.signup = function() {
        Authenticate.emptyLocalStorage();
        if (!vm.isSending) {
          vm.isSending = true;
          vm.regSubmit = 'Registration Processing...';
          var userReg = { mail: vm.email, pass: vm.password, field_full_name: vm.fullname, field_date_of_birth: vm.dob, field_country: vm.country.code, field_state: vm.state };
          UserService.registerAcc(userReg, function(response) {
            if ($rootScope.isRegistered) {
              vm.regSuccess = true;
              vm.regError = false;
              vm.isSending = false;
              vm.regSubmit = 'Registered';
               $state.go('login');
            } else {
              vm.regError = true;
              vm.isSending = false;
              $rootScope.isRegistered = false;
              vm.regErrErrText = response.message.mail;
              vm.regSubmit = 'Register';
            }
          });
        }
      };

      /**
       * Fetch State Based on Country
       */
      vm.updateCountry = function() {
        angular.forEach(vm.stateList, function(currentState) {
            if (currentState.country_id == vm.country.id) {
              vm.availableStates.push(currentState);
            }
        });
    };
      /**
       * @param  {[id]}
       * @param  {[currentpass]}
       * @param  {[newpass]}
       * @return {[success]}
       */
      vm.changePass = function(id, currentpass, newpass) {
        UserService.changePass(id, currentpass, newpass, function(response) {
         alert(response.message);
        });
      };
      /**
       * @param  {[email]}
       * @return {[type]}
       */
      vm.forgotpass = function(email) {
        vm.forgotSubmit = 'Submitting...';
        UserService.resetPass(email, function(response) {
          if ($rootScope.Changedpass) {
            vm.forgotSuccess = true;
            vm.forgotSuccessMsg = response.message;
            vm.forgoterror = false;
            vm.forgoterrorMsg = '';
            vm.forgotSubmit = 'Sent';
          } else {
            vm.forgotSuccess = false;
            vm.forgoterror = true;
            vm.forgotSuccessMsg = response.message;
            vm.forgoterrorMsg = response.message;
            vm.forgotSubmit = 'Continue';
          }
        });
      };

      vm.getuserDetails = function(){
        UserService.getUserById(getcurrentUser.userid).then(function(response) {


          $rootScope.$broadcast('USER_PROFILE_LOADED', response);

          if ( !response.data.user_picture || angular.isUndefined(response.data.user_picture[0])) {
            vm.prfPic = null;
          }else{
            vm.prfPic = response.data.user_picture[0].url;
          }

          // Null check all field values
          // As there may be accounts which has not submitted these values
          vm.fullName       = response.data.field_full_name.length  && response.data.field_full_name[0].value;
          vm.followers      = response.data.field_user_followers.length  && response.data.field_user_followers[0].value;
          vm.following      = response.data.field_user_followers.length  && response.data.field_user_followers[0].value;
          vm.updateName     = response.data.field_full_name.length  && response.data.field_full_name[0].value;
          vm.updateCountry  = response.data.field_country.length  && response.data.field_country[0].value;
          vm.updateState    = response.data.field_state.length  && response.data.field_state[0].value;
          vm.dob            = response.data.field_date_of_birth.length  && response.data.field_date_of_birth[0].value;
          vm.updateInterest = response.data.field_interests.length  && response.data.field_interests;
          vm.viewprf        = response.data;
          vm.actualId       = response.data.uid[0].value;
          vm.useruid        = response.data.uid[0].value;
          if (vm.viewprf.field_interests.length === 0) {
            vm.showModel();
          }
        });
      }
      vm.updateProfile = function(getInterest) {
        UserService.updatePrf(vm.actualId, vm.updateName, vm.dob, vm.updateCountry, vm.updateState, getInterest, function(response) {

           if(response.uuid){
            alert("Updated successfully");
              vm.modalShown = false;
              vm.isReadOnly = true;
           }
        });

      };

      vm.editable = function() {
        vm.isReadOnly = false;
      };

      vm.proceedInterest = function(interst) {
          vm.getIntid = [];
          angular.forEach(interst, function(data) {
            vm.getIntid.push(data.tid);
          });

          vm.updateProfile(vm.getIntid);
      };
      vm.categories = function() {
        UserService.getCategories().then(function(response) {
          vm.interests = response.data;
        });
      };
      vm.checkBox = function(list, checked) {
         if (angular.isDefined(list)) {
          if (checked) {
            vm.lst.push(list);
          } else {
            vm.lst.splice(vm.lst.indexOf(list), 1);
          }
        }else{
          vm.lst = [];
        }
      };

      /**
       * Sending Other user profile name
       * @return Other User Datas
       */
      vm.otherProfile = function() {
        vm.userid = $stateParams.userid;
        UserService.getUserById(vm.userid).then(function(response) {
          vm.getuserData = response.data;
          vm.getuserfollow = response.data.field_user_is_following[0].value;
        });
      };

      vm.postFollow = function(otherId){
        UserService.FollowPost(otherId, function(response){
         vm.getuserfollow = response.field_user_is_following[0].value;
        });
      };
      vm.postunFollow = function(unotherId){
        UserService.unFollowPost(unotherId, function(response){
         vm.getuserfollow = response.field_user_is_following[0].value;
        });
      };

      /**
       *  Logout
       */
      vm.logout = function() {
        UserService.logout(getcurrentUser.logouttoken, function() {
          $rootScope.isLoggedIn = false;
          Authenticate.emptyLocalStorage();
          $location.href = $window.tapConfig.host + 'user/logout';
          $rootScope.$broadcast('USER_LOGGED_OUT');
        });
      };

      /**
       * Change Profile Picture
       */
       vm.changePic = function(getCurrentId) {
          var getbase64 = "data:image/png;base64," + vm.myfile.base64;
          vm.prfPic = getbase64;
          UserService.changePicture(getCurrentId, getbase64, function(response) {
            alert(response.message);
          });
      };
      /** Rest Account Password */
      vm.resetwebPass = function(password){
        var getUserId = $stateParams.userid;
        var getTimestamp = $stateParams.timestamp;
        var getHash = $stateParams.userToken;
        UserService.change_pass_web(getUserId,getTimestamp,getHash,password,function(response){
            $rootScope.$broadcast('USER_LOGGED_IN', response);
            $state.go('landing.trending');
        });
      };

      /** Take Pic from web camera */
      vm.openWebCam = function(getPath){
        $location.path(getPath);
        vm.camera = true;
      };

      /** Disable web Camera */
      vm.disableCamera = function(getCurrentId,getCam){
        UserService.changePicture(getCurrentId, getCam, function(response) {
            alert(response.message);
            vm.camera = false;
            $state.go('landing.userprofile');
        });
      }

      /** Social Login */
      vm.socialLogin = function(getsocial) {
        $window.location.href = $window.tapConfig.host + $window.tapConfig.apis.user[getsocial].url;
      };

    });

})();
